<?php
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}
?>
<!DOCTYPE html>

<html lang="zxx" class="js">

<head>

	

    

	<meta charset="utf-8">

	<meta name="author" content="ABEE Foundation">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="ABEE Foundation">

	<!-- Fav Icon  -->

	<link rel="shortcut icon" href="images/abee3.png">

	<!-- Site Title  -->

	<title>ABEE Foundation</title>

	<!-- Vendor Bundle CSS -->

	<link rel="stylesheet" href="assets/css/vendor.bundle.css?v=<?=time();?>">

	<!-- Custom styles for this template -->

	<link rel="stylesheet" href="assets/css/style.css?v=<?=time();?>">

	<link rel="stylesheet" href="assets/css/theme-java.css?v=<?=time();?>">

	

</head>

<body class="theme-dark io-azure io-azure-pro" data-spy="scroll" data-target="#mainnav" data-offset="80">



	<!-- Header --> 

	<header class="site-header is-sticky">

		<!-- Navbar -->

		<div class="navbar navbar-expand-lg is-transparent" id="mainnav">

			<nav class="container">

				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./">

					<img class="logo logo-dark" alt="logo" src="images/ABEElogo.png" srcset="images/ABEElogo.png 2x">

					<img class="logo logo-light" alt="logo" src="images/ABEElogo.png" srcset="images/ABEElogo.png 2x">

				</a>

				

				<!--<div class="language-switcher animated" data-animate="fadeInDown" data-delay=".75">

					<a href="#" data-toggle="dropdown">EN</a>

					<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

						<a class="dropdown-item" href="#">FR</a>

						<a class="dropdown-item" href="#">CH</a>

						<a class="dropdown-item" href="#">BR</a>

					</div>

				</div>-->

				

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">

					<span class="navbar-toggler-icon">

						<span class="ti ti-align-justify"></span>

					</span>

				</button>



				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">

					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".9">

						<li class="nav-item"><a class="nav-link menu-link" href="#intro">ABEE<span class="sr-only">(current)</span></a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#tokenSale">TGE</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#roadmap">Roadmap</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#apps">App</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#team">Team</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#faq">Faq</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#contact">Contact</a></li>

					</ul>

					<ul class="navbar-nav navbar-btns animated remove-animation" data-animate="fadeInDown" data-delay="1.15">

						<li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" target= "_blank" href="https://token.abeefoundation.org">ABX Tokens</a></li>
                        
                        <li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" target= "_blank" href="x">Pre-TGE / Whitelist</a></li>

					</ul>

				</div>

			</nav>

		</div>

		<!-- End Navbar -->



		<!-- Banner/Slider -->

		<div id="header" class="banner banner-full d-flex align-items-center driver">

			<div class="container">

				<div class="banner-content">

					<h1 style="margin-top: 55px; text-align: center;">Slogan Goes Here...</h1>
					<div class="row align-items-center mobile-center">
						
						

						<div class="col-lg-6 col-md-12 order-lg-first">

							<div class="header-txt">

								<h1 class="animated" data-animate="fadeInUp" data-delay="1.55"><br class="d-none d-xl-block"> <br class="d-none d-xl-block">Form Goes Here</h1>

								

							</div>

						</div><!-- .col  -->

						<div class="col-lg-6 col-md-12 order-first res-m-bttm-lg">

                           

					</div>

                            

							<!--<div class="header-image-alt animated" data-animate="fadeInRight" data-delay="1.25">

								<img src="images/header-image-blue.png" alt="header">

							</div>-->

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .banner-content  -->

			</div><!-- .container  -->

		</div>

		<!-- End Banner/Slider -->








	<!-- Start Section -->
	<div class="section section-pad section-bg-alt" id="why" style="color: black;">
	    <!--<div class="background-shape bs-reverse"></div>-->
		<div class="container">
		    <div class="row">
		        <div class="col-lg-6">
		            <div class="section-head-s6">
                        <h6 class="heading-sm-s2">How it work</h6>
                        <h2 class="section-title-s6">Best Features</h2>
                        <p>The ICO Crypto Team combines a passion for esports, industry experise & proven record in finance, development, marketing.</p>
                    </div>
                    <div class="gaps size-2x"></div>
		        </div>
		    </div>
           <div class="row">
               <div class="col-md-4 res-m-bttm">
                   <div class="features-box-s3">
                       <div class="features-icon-s3"><img src="images/jasmine/icon-a.png" alt="icon"></div>
                       <h5 class="features-title-s3">Ultra Fast &amp; Secure</h5>
                       <span class="features-subtitle-s3">Instant Private Transaction</span>
                       <p>The Smart Asset Blockcjain os s,arter &amp; more open with lorem Ipsum  is simply text of the crypto.</p>
                       <a href="#" class="features-action"><em class="ti ti-arrow-right"></em></a>
                   </div>
               </div>
               <div class="col-md-4 res-m-bttm">
                   <div class="features-box-s3">
                       <div class="features-icon-s3"><img src="images/jasmine/icon-b.png" alt="icon"></div>
                       <h5 class="features-title-s3">Highly Scalable</h5>
                       <span class="features-subtitle-s3">Limitless Applications</span>
                       <p>The Smart Asset Blockcjain os s,arter &amp; more open with lorem Ipsum  is simply text of the crypto.</p>
                       <a href="#" class="features-action"><em class="ti ti-arrow-right"></em></a>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="features-box-s3">
                       <div class="features-icon-s3"><img src="images/jasmine/icon-c.png" alt="icon"></div>
                       <h5 class="features-title-s3">Reliable &amp; Low Cost</h5>
                       <span class="features-subtitle-s3">Instant Private Transaction</span>
                       <p>The Smart Asset Blockcjain os s,arter &amp; more open with lorem Ipsum  is simply text of the crypto.</p>
                       <a href="#" class="features-action"><em class="ti ti-arrow-right"></em></a>
                   </div>
               </div>
           </div>
           <div class="gaps size-2x"></div>
           <div class="gaps size-2x d-none d-md-block"></div>
           <div class="row text-center">
               <div class="col-md-12">
                   <ul class="btns">
                       <li><a href="#" class="btn">Get a Wallet</a></li>
                       <li><a href="#" class="btn btn-outline">Download Whitepaper</a></li>
                   </ul>
               </div>
           </div>
        </div>
    </div>
    <!-- End Section -->


	<!-- Start Section -->
	<div class="section section-pad no-pb section-bg-alt" style="background: white; color: black;">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-5">
					<div class="res-m-btm">
						<img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhIVEhUVFRUVEhYVFRAVFRUVFRcWFhUWFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGy8mHSUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKkBKgMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAgMEBgcBAAj/xABDEAABAwIDBQQHAwoGAwEAAAABAAIDBBEFITEGEkFRcRNhgZEHIjKhscHRQlJyFBUjQ2KCkpOy8CQzU1SiwkRj4eL/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMABAX/xAAlEQACAgICAgICAwEAAAAAAAAAAQIRAyESMRNRBEFhcSJCUjL/2gAMAwEAAhEDEQA/ABr6jJSsNj3lCdDki2FkNC8qkgE5kO73I1hElnDogzp7qRSzWKVSp2MW81VlBdU3KGduTxS4nKnmthDMMyIxVOSBQlTonrrhm+hXEKMnSjIoUbk9vLoUrFPOnT1PJdQJnJyklsVr2AMsCWmonpzeThOry9ddQMcXl1eWMcXl1eWMcXl1eWMcXl1eWMcXLJmorY2e3Ixn4nNHxKYfjNMNaiIX0/SR/VEGibZcshx2gpB/5MP8xn1XRtBSf7mH+Yz6rUzWghZesoAxyl/3MP8AMZ9UoYvTnSeL+Yz6omtEyy5ZR24jCdJoz++36p1tQw6PaejgsbQqy4QlXHNeWMIISSE5ZcIWANELlk4Qk2RMYnu3CVCSF0C9k82JeHZQdicpcb1GjiKlMiQ4hJcb1OgkQxoUhhKCVMyC7JFIjlCDdon4JFSElYQ7C9POkQ+CVSbrtjl0K0IeVxj0qyUGKUpuxkiXT1JClMq0NASHSWVo5vYriG2ThL7cIC2oTgqFZZExaDrZE4Cg8FUp8cydOzEteSGuS7rGPLyH1uLxx5D13ch8ygVXickmRO6OQy8zxTxxtiuaQeq8WjZlffdyb8ys62x2wle50Mb+zY24k3DYud93e1sONrKVtBiHYQucDZxsxn4nZXt3ZrPKuYNF+NrknUk5+9GSUdGjcjz2E5uOZzzOZTVx94DwQmeuJvnqorq8N1d4an6BJyKKKDj43n2Xs+B8rKDOyoGhy5kAjzChxY0OoROmxYHhbnyS2w0ibhmDSzMcWvLnAZBrbpP5kqwDannfb/1PaD/xViodtKqOIRxCPeHslzNRyuLAnqhcnpDryTeZjTxG40W7s0ObQHErdWZ2e1G+I5+rKzdJtrY8Uw2vltfIcsiFdINtZpRuVUUVRGcnCwDuo71oeybaU07GRhpa29g8AuFyTY36oObNxMPixuobo9w6PcPgVNh2trW6TS/zXn4lby/DKY6xRH91ijybOUTtaaE/uMW8jNxRjkPpCr2/rpPEMd8QiMHpWrG6kO/FGP8ArZaTLsVh7v8Ax2DpcfBQ5fRzh7tI3N6SP+q3kZuKKrT+l+T7cUbvF7PjdTh6X2/7cfzf/wAqZUeimld7MkrfFjviFCPoij/3Dv4Go+QHAEUzbKaxqjxsyUiMWC8udDE+nYE8WAFR4XZJbZcwpsJMiiulllinIY8kzM+xQktGOlicZEV6nzRGNgSRTMMQXCnRvUd+SSJlVToIQYnVBjnThnVVNGHXvUKokSpJVDnelcjCDUJ+OZC3vsU9HJkjCTsDQXinsp1HU3QGMl2nmpTHECwK9HBGUv0Rk0iwS4m1gzzPIfNCqzFJJMr7reQ+ZQusq2RDekcGjv1PQcVTdoNu2xgiMhnebF56N0HiuxRUSdtlvra6OEXkeG8hxPQalU7HduwwER2jH3nWc89G6D3qgV+OzTElpLQdXuN3HxKFktBufXdzdmhLJ6HUA3Pjkkzt5oJ/9khJPfa+QXq97yG53JA8bCyhYTSTVLj2bHOaxpdIQPVawC5JOg0RyR4LhG3uBPyCjJ32UivQJp8GnksGtJvyBR6g9HUr7GRwZfhqfctGwLDmxxNAGdszxPNFGMAXLLI/o6o44/ZmFX6NbD1Jc/w3CqVXQzUsm5KMuDhoV9BtjCqW2eENmjLbZ6t7ilWSS76GeOL6KDh9TlzaeH05Kz4IKK7vyuFj7jebIRc5ZWd9VRsKcQ50bvaaSrBTPuLeI+YV2c5dqauwhvsxwD90ItDtLQjJpjHQALE8Updx+Xsu9ZvQ8PBQ7ocBbN/GPUTvtR+YC859G/SS34ZXD4FfP5K8JCOJ8ytwNZ9BNw6A+zUSjpKT8ShG0eB1Ijc+lrHktF9xztQOTgsbpqmS+Urm9+8USh2jkiBDZXvJFsybJeAbLng+KVVORJNVOdza4gjorK30o0vEG/HTXisQrMQkl9px6KJZFQNyNYpBcBTw1cgp7BOSlebNqzUNONk5Rm7wO9Ry66VSHdeD3pQMuEbBbRBsVcA/wRaN123Vbxd531afQbJNNVAFFI6sWVXbJZSY6rvXNJ+gWG56tMsnug76olSIZSlUWGwo6WyQ2pKiySZJuncbqiRgxE4lcnjSqRyeeLqkMcpuombS7AcsRvYC6nU1FbN3l9VLa0D5lAsb2pigBAIe4d/qjqV62D4kce5bZCWRvoNzStY27iGgcTkFUsf23jiBEZH4nf8AVvFUPHtsZZ3EMO936NHQKtym53pHF7u/RXlkS6Aoewxim0k1Q4lpIB1e7Xw5IO7dBuTvu5nNI7RzsmhG9ntlZqp9mNvzccmN6lScm+ylJAbec82Cv2x3ouqKktfO0xRHO7vaI/Zbrfr71o2xuwFNSWe8CaUcSPVaf2W/M5q9NWoVyvooe2lNDhuFyR07AwOswnK7rmzi48TZZPsLB21TvOzDc/3lpXp3lIo4wPtTNb7ifkslwfGDSxvayxe53tg5NFrWFxmdc9OqWfQ+LTNupDbVPCdpWAzY5K43Lz4vf9Uf2X2v7L1XtfLfRodfPrwHgueUJLaOqM19mrV+Ow043pX2HAak9wCqeI7ZsldaKKw+9K9kY/8Aqp+2O0hqHANh7EAXzdvOPQ2FlVROeQ8cyjHHa2CWSnoOYyHsqe1bHkTd3ZntG2OvrAZIrTyXJI4ZjvuL/VU1zr56dFZMIkyYf2beViPiVSqVE7tkzE4O0YbZkDfb0PtD4FVlzla4pN02P2T/AMXfTP3JWxGB0z6+SKqtubodEDobk3Hhp4Job0JPSspznJJcV9D02y2Fs0ji8Q0/FLk2Owt/6mLwAHwVOLI+Q+dFNoYWPNnHd5LeHejzDTpG3wJHzS4fR1QDSIHqSfiUrixvJ+DEfzYzTtB81z81t5u8ivoWj2apYvYhY3o0KX+bovuN8gtw/IPI/RQmyKPO9csmpDkvESst2dY6yn4cwOkaO9B3OIUzBam0rCeaqo7MzQWUgsqjtZDuSN7wroyW4VQ2tdvPaOSvmS4goANcEpsZKXFT8VKY22q5KMkJggU2OEBIjmASnThAYeMS6yLPIZp6kiLs9BzRBrA0fErt+P8ADlPctIlLIl0NU8Nhn5JnEcSjhbvSOtyHE9Age0u2EVOCGkF3PgFlGLY/NUOJ3iAftHXw5L00oY1USdOXZa9qNvCbsZlya05n8RVFqp3ym8rrDg0aKOXNbpmeJ4rjInPKRzbKKKR103BgT9Jh7nkCxcTwCO4Ns051i71G8zqeiueHUMcIsxtuZ4nxUJ5VHoNgjAdjhk6bL9kfMrSMLa2JoaxoaBwCB0z7otC5TjkbdiSLTQz3CKRoDhT0fiXUnoVGZ+nxh/JKdw4T5/wOssQpWF4v5r6C9NVJv4aXf6crHeBu0/1LCtm4t6bcvu3BzsCAeBI5fVLJlsasHupSjWxtC99ZAxv3w5/4G+s6/dlbxCI4phL2G5EZvkC1z/6S3LzUzZuiqIX9rDHvPItvG4a1twXc7k2HkpuaouoUyb6VsDLZGVTfYLRHJ+yRfdd3A3I625qitoyTotVDquc/pHMYwgtc2VoLXNOWWar1dsg6MDdkBbwzcMu6xskjkSWx3j3ZSpaQgkchc9w5lFaRu6IweTQepYiFZSNZGGNbbfIDjxNjndDqib9IRwa9vyb/ANkVLkJKNBCodmHc8j7vhkoGPQb8bZBfeZrbW3suH9J8SpjnXO6eOXjqPiV6lN2ua7PW4/4vHiCCmToRqyuUocftO83IgHvGkjx0e8fNP02FP3ixrS8j7oJNuBsE+cIn/wBGT+B/0TWS0MxYtUs9molH77j8VathNpa11XFG6YyMcbPDgNLE3BHRV5uA1B/UyfwP+ivno02Yljm7WVhaADa4IzPXxTJuxZKNGqWXLJZXFQQyx7uCadImauVMRuJzXjRidKHwzeUvDaez2k81FprojG+1kW6Zi2RykCyEYrFvODlKp6gEd6iYjKjknoxBkaAEMrJSNEQ3rrjKAvyte6nBOTpAbAlNUOcbWJPBWrDcLyDpPAfVSMOwtkWdru58uijY5j8dO03ILuX1XrYfiRh/KXZzym30EaysZE3eeQ0D+8lR8X2kfPdsZ3WDXmeqqeMbQTVcm4wk3Oo0A7k/VvEEQaOGve4rpc9WZRBWNU3aFxDr7g3nXPuCr/al2TVOqt553OZu7vPJW/ZPYntLPm9VnBvE9Vyymo7ZUrGCbPSzusxpdzP2R4q/YdsqyAXd67/cOgV0paOOJgZG0NA5KLVNuuPJmcuuglbmekB10RnpO5RfyeymhRdO4hFqSUlDoYkSpRZWhVissmEOVkhVfwvOysEK7V0LEFba0XbUNQy1/wBG5w6s9b5L5cu6OU2JaQSLjJfXzmggg5gix6HVfK+2eGmCoe37j3MP7pyPlZKysC34L/iY2PuASM+5wycPML1XTTdqYqqd0UIsYuxbcSa3Lha4IyyzVf2YxQRC1/Vcd4/s6C/gbg91uS0OCNtQzdcA8cL2IXNJcJHdhyLsGOwrDG71paqTIWF5ATa987AZ21KF0OBRTTF0Ikiga4k/pHnfsbga2I566KzM2RiBBdY8bEucPImymVsbImGx4dMkJT0dEsiapNv9spO0Jb2oDRYMB91yqRFJvb555/E/RWPFKsPc+x4OHmCB8VVqI2Nv75qmNaOKb2HGy7zQRrlb+/NSQbODxo7Xrb5i/khVK42I628Mx8EQgdcFvi3u5eRTCGleiqri33wva3tNY3EC7mgezfpn5rTewb90L52w2udHI2Rp3XMcLW4C+XgDceK3LZbaBlXCH6OGTxlk4ag/HvBVYPVEMkadhbsW8guhoGgSyklMIJK4oGNY3BSt3p5AwcAdT0GpVUd6U6O+kh791Mk2ABdjvG6lQ0WWido2iyIRtAC8a9HSgc+ANGSZZFchT6lNRahR8mzNhalpslGxOOynxSCyamj3tdFWOKWZ8YiykkgXS0hcfiUYjYGD4lNyStjbckNAWf7X7ZgAsYctMtSvaw4IYY/n2c7bkGtqNr2QgtYRfi76LO4o6ivkyuGXzceKfwXAZat3azXazUDmr9DTshjs0BoAV4wctvoWUlHS7K/+b46Vm6wXdxdxVWq3umk3WC9sh14lFdosQOfM5BStmaLs2bxHrO9wXL8jKl0VgqW+x/AcBZFZ8nrP17grfS1CBtuSiMDTZefN8tscKPqrBR/yxpQ6qcQhH5UbpKNZZnShRpHZoSysSzWoUYLxFTI0Ep6q6KwS3Ri2mBlnwh+iskGirOCBWMTtbYOcBfS69GDtE0Slh/pewf8Axbt0f5rGPb3uA3beO771seIYpHC3ee6w4cSegWdek2sinbC+N13NLmki+QdYg+Bb71p9FYdmK4ZKGv3XHdBNgfuu0G93cCj9Li01FLui7WC12Ou7cvydxjPAqRtlsw7s2V0LbxTC8zR+rlGTx0JBI8UDw7G2hgiqWGSMZRyNt20QPAXyc39krUmqYba2i/Hax5bcRg94N0Iq8Rqqs7rGPIGoja5x8bDJFdhtl/yob7JQ6mabbzWvY5x/0w1w9U55nMC9lq9HRRxM3IWBrG5AAEZjnqTfmc1JYt7LvLaMIh2Yqx6xppQD95pGXihE+BTNc4iN1g7kcicwDy8V9IbgN7tsOZsL9eXBBMU2ejed8gxOt7bDmM9N0ghw0yIT8K6J877MIkiLMyCDcZEEdPgn4H2PvHQ8PgrltTsvIwFxZvNt7bAbW5vj9pnvHRUdrCD2btfsngRqlGJ8rt0h32T6ruh0PlbxBRnZ3HH0c3aD1m5CVv32cHD9oXy68kCpXhwMbuWXM8vH/wCpFO4g7p9pml9HM5LJ0Bqz6RwyvZNG2SNwex4u13Hoe9N49i8dJC6aU2a0ZDi48AO8rIdgtqvySTspHf4eU3BP6p/Ppz81E9Iu1RrJtxh/QxEhv7TtC/6K8P5HPJU6AO0WNyVkzppTmfZbwY3g0IXdeJSbqwDX6d6Is0QWlluUTqalkbd57w0DiTZeCotukdSG6pyidqb5ITXbW0gd/nNPRO0G1VBqZ234A3yT4vhSyT3pEpyrotNGw2u7Lu+qXW1zYmlzjZB37UU26XMmY/uDgs62m2lknfuMuScgBwXtQhDFGonPuT2T9rdr3PO4zPgAE3sxss6Qieoz4hpUvZTZQMtNN6zjmAVdoouACrGF7kJKdaiJggAAAFhwAQnaSq3RuDxVnLREwvdwCy3afEi4uN83GwQyZKWjYoWyFRwmpqP2W/JXWGHhZR9lcI7KAEj1n5nojDYbLxcs+UjqG4oFMijUd0ll6CsF1KmEcqqe4Vdq6Ygq1veCEFxKPisgMDgc040pt5Xmu71RIBJZcIvQyIRHO22bglzYqyJhfe/JGMJSdJGZo+FVDWNBc4Ad5AQLbTaWjduNbM10odkGm5HO9tFk2JY3NUmxc4NvYNB1TlZsZWNgNRuBobnYG7rc138ajQsdOzQHPdI4PLy7KwuSbdExjUN4Xd1neRVd2axCWMNZOxzCRdu8LXHcrYXh7HDW7SPcudtp7OxU1o7sLiNt6CQB0bxvWOYzyd4cfFQdpfRrDK/fpZBE5x9h2bLnlyQnCKjdcLZEHd8DkrPsfXzSVZjlaSGNfICMxlZo97x5J4t1Rzzi1K0XHBqFtPTshjA3I2bgtx+8TbiXXJ7weaJRlxyB0y0PuPFRGZFpyGoPfrfInuPmnXN5HW4B5AZWPmVQY9Jndj3HuIy5ndJ8P74sSboFmusWm9jctyFt2xPJ1x4FL3gLg62zsBkG3z3eR3Qlua149XUG4ta7Ta2vS4RQGQ3Qm5bqXEkWJG8chYmxAGZPG+ZHdRfSPsa1wNRTts4ZvaBrbMuFuPErQn3c/wC6AbnoW2yI7zqDz8V1FnD3rNWC6PnBji/ukGY5Hhfx4+BS5yXAPb7TdRxy1HUf3orDtzhTI6hxZYMd67bWux5vvN6Eg5KtHevfLhfPW2h6qfFlLQoy3Fxoc7d6bKZse0PrAt4NHBOuXTBUiE3bEOSEu10ncRAaRhLcnyONmRi57yBeypuMOmqhJO65hjI3s8m7191o8lddoT2VIWN1ebdd4/QJezuD9rglUG+1JJK5vWGzAP8AgfNTx41GGu2ZytmSsibfJo8kQmoI3s3msALcnWChhtu4onhdSLkO48+aA7ANRQ2zarV6PYAZf0jb3aSxxHEaruF0ze3DXC7Xg26qwbKMBEbQM2SyN8LlNB1NCzVxZbY2XRSkp7ZnVcggDRcqHW4yGZNzK6ZNy0jkSBm2mJbo7MHvPyWf4RSmpqgNWszK9tHi5fNIb3GnijXo7YNx7retvZlcWeetHVCPFFyaLCwSHLu+kSS2Xm0OQqsIaCQ66K1EgKF1U7Gi5Nk8fQAvTyXCgYtiMbB6zgqriW0pFww2CqtXibnnUkq0fjfcjFlxDHQfZyQSoxs80IcSdSkbgXVGKXSNQVoqySaQMBPf0UzEp95wjB9VuSYwMBkcknG1gm4GOO84C9tSq9IWrYxVVxje3cyLdFbtnvSlNFZk7RLHlfQO+hVHNO+QktY53Rrj71IjwSc/Y3fxOaPde6WrC0j6OwPGKDFIwQGPLdWuA3mnvBXsV2Yjb60HqHkPZPgsJwCjnpZWzMmawtOYbvHeHEEZZLRZfSJMQGtiZfmS51/AWSyx2BNxeitTxlkr2HIteR9PgtI2ThG854z3omjIj7Wbhnrp7lmtbPLLI6VzCC4hxyLRcZZXXGV84G6HuaOQcQPIJY4ZJlXlVG0yTtaLulbHrfecGm1h35c0Lqtr6Rl96cEkZBgLiNc8sr/VZA4HVx/vqVwPb18b+4KviE8ho9V6RIRbs4nmxOpa0cOptqfFBKrb6a94omM5k3cT106qrCN59lh8rfFLGHyHWw6n6KixfgR5AjWbY1jzftQzK3qNAuL3sb3vnfzKEVOLzv8Abmkd++63kpAwrm7yShh7Brcp1jYvkAz3E659c006De4I+Kdg4LxsOCPjBzALKG2jbJFdSuDL9Ecc5D8RN91vNw9yzgkjKTbExUdgL8kr8nan3FN3R4oHJh3aPGWucxt79m8Od1BsB5fFaH6LrOw2PiC+a/jK8n4rGdtcNdBO6QA7kp3geAcfaHnmrH6Mdv4qJjqeq3mxOeXxyAFwYXW3muAztfO/eVzPTplPoK7VbKxCpls0jeIeLA29bXTvuqvWbPNjIIJ553+a2CLazDJiCKunJOXrSNafJ1ihm3tfSChldG+N7i2zNx7CSTpok4btMZT+qMjpKkOlBH2He7RG9k8TjhkL5XBrRI/M6XOiB4FhxMMtRvACM5tJFyLXJA8UKnmuwDm8uTVRnuzVa/amJ/syAjQW4oFiOKAXsVRakWjBHPJcgndYkkmw+Kp5tVQixbOSybzieZurvsZUBkLswLu+CocRUuimlAs3RRSi/wDropK60aacTaPtBNSYoz7yoTe2OpA8VIhjI9p9z3J1jxf5JVL2WXEcZY1uRuVTMUxkvOvgif5CH/Ye7zsn4cLDdI4297i2/wAyj41/VDLXZURG9/AnoCVIjwqY6Rkd5IHxVwEIGsrR+EE/GyXHHGdO0k/DmPJoJ96Kxh5lVZs9Ifaexv8AE4/AJ+PZ6Me1K4/hDW/G6tsdGeFOT3ut8JHfJS2Uc3AMYO51vcxtvenWIV5CvU2FtazcbE5zdbuD7eJ0UqGlc0WbuRjuLfgy5Rj81uPtSjwZn5uJ+CU3CmcXPd+9u/0gJ1iYnNAg04+1IT+FuXm4j4JvehBtm48i+/uYAfejww+Efq2n8Q3j5uunxYZAAdAAn8QOZXmNJ9iA/wAs/GVPtgqDoA0ci8N9zAUZLkkuTLGhXMEjCXn2pGjo0k+ZPyTjcIb9p73eIH9ICnuckOcjwQOTI7cPiH2AeufxTtmjQAdAFwlNkpqQLFOkTbpFwlNuWMce9NOclOSCgEQ4ppxTjk25AI25QB60pPBot4lTpDYEqJSN9Un7xukfYyHHJF0pyQsYuGKUBey0rg659nVDqvZelZGSI/WtqHOCOYl7TOv0SKz2HdCi4picmjNpcMb2m6cm31sDkoOIUJjd6rQ8cCArTNw6LkK53jRfkyowMeb+rujjqvNJJzVoqtCq7NqpyjQ6lZJrRaJnUqMxnq5cSl1f+WzqnIdEgwqkiA1uegRCMAfZPiQFEi1XW6qiihWwpTi/stZ4klPy77BcuA/CAomF6pzGfsqqSqybewhgkH5Q4h3abo1NxbporBHgsA+xvfic4+66hbH/AOSfxI6rxiqJSk7GIqSNvsxsHeGtv5p666klUSFEkpJSkkrGElJKUkFYxwlJJXSkrGOFJKUUgrGElIKUUkrGEOSCluTbkDCCkFLKSUDDZSCE4UgoBGnBNuTrk25AJDr77tuZslBlgBySqz7H4l1yT7GGXBIsnXJCxj//2Q==" alt="graph">
					</div>
				</div><!-- .col  -->
				<div class="col-md-6 offset-md-1">
					<div >
						<h4>Hkjhjkhm Lsdgkjsfg</h4>
						
						<p>While existing solutions offer to solve just one problem at a time, our team is up to build a secure, useful, &amp; easy-to-use product based on private blockchain. It will include easy cryptocurrency payments integration, and even a digital arbitration system. </p>
						<p>At the end, Our aims to integrate all companies, employees, and business assets into a unified blockchain ecosystem, which will make business truly efficient, transparent, and reliable.</p>
					</div>
				</div><!-- .col  -->
			</div><!-- .row  -->
		</div><!-- .container  -->
	</div>
	<!-- End Section -->
	
	
	<!-- Start Section -->
	<div class="section section-pad section-bg" id="benifits" style="background: white; color: black;">
		<div class="container">
		    <div class="row">
		        <div class="col-lg-6">
		            <div class="section-head-s6">
                        <h6 class="heading-sm-s2">what you get ?</h6>
                        <h2 class="section-title-s6">Benefits</h2>
                        <p>The ICO Crypto Team combines a passion for esports, industry experise &amp; proven record in finance, development, marketing.</p>
                    </div>
                    <div class="gaps size-1x"></div>
                    <div class="gaps size-2x d-none d-md-block"></div>
		        </div>
		    </div>
            <div class="row mb--x8">
               <div class="col-lg-6">
                   <div class="benefits-item">
                       <div class="benefits-icon">
                           <img src="images/jasmine/icon-d.png" alt="icon">
                       </div>
                       <div class="benefits-txt">
                           <p>ICO Crypto makes <strong> you the sole owner of a secure </strong> decentralize registry for your collection an products</p>
                       </div>
                   </div>
               </div><!-- .col -->
               <div class="col-lg-6">
                   <div class="benefits-item">
                       <div class="benefits-icon">
                           <img src="images/jasmine/icon-e.png" alt="icon">
                       </div>
                       <div class="benefits-txt">
                           <p>The registry is a <strong> tamper-proof, append-only ledger </strong> that uses state-of-the-art cryptography.</p>
                       </div>
                   </div>
               </div><!-- .col -->
               <div class="col-lg-6">
                   <div class="benefits-item">
                       <div class="benefits-icon">
                           <img src="images/jasmine/icon-f.png" alt="icon">
                       </div>
                       <div class="benefits-txt">
                           <p>Provide your customer with a <strong> Lorem ipsum dolor sit amet, conse ctetur sed </strong> eiusmod tempor incidid labore et dolore</p>
                       </div>
                   </div>
               </div><!-- .col -->
               <div class="col-lg-6">
                   <div class="benefits-item">
                       <div class="benefits-icon">
                           <img src="images/jasmine/icon-g.png" alt="icon">
                       </div>
                       <div class="benefits-txt">
                           <p>ICO Crypto the prowess of blockchain <strong> labore et dolore occaecat cupidatat </strong> non proident, sunt in culpa qui officia </p>
                       </div>
                   </div>
               </div><!-- .col -->
            </div>
        </div>
    </div>
    <!-- End Section -->
    


 


	<!-- Start Section -->

	<div class="section subscribe-section-s2 section-bg section-dark" id="join" style="background: linear-gradient(180deg, #FFF 50%, #15257b 50%);">
		<div class="container">
			<div class="subscribe-box animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="heading-sm-s3">
                            <h5>Join our mailing list for updates</h5>
                            <p>Don't miss out on the token sale! </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="gaps size-2x"></div>
                        <form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form inline-form-s2" abineguid="6B8B90FBC9F0474583EFF0BF32C69E8A" novalidate="novalidate">
                            <input type="text" name="youremail" class="input-round-s2 required email" placeholder="Enter your email address" >
                            <input type="text" class="d-none" name="form-anti-honeypot" value="">
                            <button type="submit" class="btn">Keep me updated</button>
                            <div class="subscribe-results"></div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>

	<!-- End Section -->
	

	

	<!-- Start Section -->

	<div class="section section-pad section-bg-dark" id="partners">

		<div class="container">

			<div class="row text-center">

				<div class="col-md-6 offset-md-3">

					<div class="section-head">

						<h6 class="section-title-sm animated" data-animate="fadeInUp" data-delay=".0">As seen in</h6>

					</div>

				</div>

			</div><!-- .row  -->

			<div class="partner-list">

				<div class="row text-center">

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".1">

							<img src="assets/images/PBJ_logo.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".2">

							<img src="assets/images/CN-LOGO.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".3">

							<img src="assets/images/New-Dash-Force-News-Logo.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".4">

							<img src="images/partner-xs-d.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".5">

							<img src="images/partner-xs-e.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".6">

							<img src="images/partner-xs-f.png" alt="partner">

						</div>

					</div><!-- .col  -->

				</div><!-- .row  -->

				<div class="row text-center">

					<div class="col-md-10 offset-md-1">

						<div class="row">

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".7">

									<img src="images/partner-sm-a.png" alt="partner">

								</div>

							</div><!-- .col  -->

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".8">

									<img src="images/partner-sm-b.png" alt="partner">

								</div>

							</div><!-- .col  -->

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".9">

									<img src="images/partner-sm-c.png" alt="partner">

								</div>

							</div><!-- .col  -->

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay="1">

									<img src="images/partner-sm-d.png" alt="partner">

								</div>

							</div><!-- .col  -->

						</div><!-- .row  -->

					</div><!-- .col  -->

				</div><!-- .row  -->

			</div><!-- .partner-list  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section -->

	



	<!-- Start Section -->

	<div class="section footer-scetion footer-particle section-pad-sm section-bg-dark">

		<div class="container">

			<div class="row">

				<div class="col-sm-6 col-xl-4 res-l-bttm">

					<a href="#">

						<img class="logo logo-light" alt="logo" src="images/abee_logo.png" srcset="images/abee_logo.png 2x">

					</a>

					<ul class="social">

						<!--<li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="#"><em class="fab fa-facebook-f"></em></a></li>-->

						<li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="#"><em class="fab fa-twitter"></em></a></li>

						<!--<li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="#"><em class="fab fa-youtube"></em></a></li>-->

						<li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/abeefoundation"><em class="fab fa-github"></em></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="#"><em class="fab fa-bitcoin"></em></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@abeefoundation"><em class="fab fa-medium-m"></em></a></li>

					</ul>

				</div><!-- .col  -->

				<div class="col-sm-6 col-xl-4 res-l-bttm">

					<ul class="link-widget animated" data-animate="fadeInUp" data-delay=".8">

						<li><a href="#intro" class="menu-link"><strong>ABEE Foundation</strong></a></li>

						<li><a href="#apps" class="menu-link">Apps</a></li>

						<li><a href="https://token.abeefoundation.org" class="menu-link">Join TGE</a></li>

						<li><a href="#tokenSale" class="menu-link">TGE</a></li>

						<li><a href="assets/documents/ABEE_Foundation_Main_WP.pdf" class="menu-link">Whitepaper</a></li>

						<li><a href="#contact" class="menu-link">Contact</a></li>

						<li><a href="#roadmap" class="menu-link">Roadmap</a></li>

						<li><a href="#team" class="menu-link">Teams</a></li>

						<li><a href="#faq" class="menu-link">FAQ</a></li>

					</ul>

				</div><!-- .col  -->

				<div class="col-xl-4">

					<ul class="link-widget animated" data-animate="fadeInUp" data-delay=".8">

						<li><a href="#intro" class="menu-link">What is ABEE</a></li>

						<li><a href="#apps" class="menu-link">App</a></li>

						<li><a href="https://token.abeefoundation.org" class="menu-link">Join TGE</a></li>

						<li><a href="#tokenSale" class="menu-link">TGE</a></li>

						<li><a href="assets/documents/ABEE_Foundation_Main_WP.pdf" class="menu-link">Whitepaper</a></li>

						<li><a href="#contact" class="menu-link">Contact</a></li>

						<li><a href="#roadmap" class="menu-link">Roadmap</a></li>

						<li><a href="#team" class="menu-link">Teams</a></li>

						<li><a href="#faq" class="menu-link">FAQ</a></li>

					</ul>

				</div><!-- .col  -->

			</div><!-- .row  -->

			<div class="gaps size-2x"></div>

			<div class="row">

				<div class="col-md-7">

					<span class="copyright-text animated" data-animate="fadeInUp" data-delay=".9">

						Copyright &copy; 2018, ABEE Foundation.

					</span>

				</div><!-- .col  -->

				<!-- class="col-md-5 text-right mobile-left">

					<ul class="footer-links animated" data-animate="fadeInUp" data-delay="1">

						<li><a href="#">Privacy Policy</a></li>

						<li><a href="#">Terms &amp; Conditions</a></li>

						<li>

							<div class="language-switcher">

								<a href="#" data-toggle="dropdown">EN</a>

								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

									<a class="dropdown-item" href="#">FR</a>

									<a class="dropdown-item" href="#">CH</a>

									<a class="dropdown-item" href="#">BR</a>

								</div>

							</div>

						</li>

					</ul>

				</div> --> 

			</div><!-- .row  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section -->

	<!-- Preloader !remove please if you do not want -->
	<div id="preloader">
		<div id="loader"></div>
		<div class="loader-section loader-top"></div>
   		<div class="loader-section loader-bottom"></div>
	</div>
	<!-- Preloader End -->

	<!-- JavaScript (include all script here) -->
	<script src="assets/js/jquery.bundle.js?ver=130"></script>
	<script src="assets/js/script.js?ver=130"></script>

</body>
</html>
