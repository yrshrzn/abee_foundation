<!DOCTYPE html>

<html lang="zxx" class="js">

<head>

	<meta charset="utf-8">

	<meta name="author" content="Softnio">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="ICO Crypto is a modern and elegant landing page, created for ICO Agencies and digital crypto currency investment website.">

	<!-- Fav Icon  -->

	<link rel="shortcut icon" href="images/favicon.png">

	<!-- Site Title  -->

	<title>ICO Crypto - Bitcoin &amp; Cryptocurrency Landing Page Template</title>

	<!-- Vendor Bundle CSS -->

	<link rel="stylesheet" href="assets/css/vendor.bundle.css?v=<?=time();?>">

	<!-- Custom styles for this template -->

	<link rel="stylesheet" href="assets/css/style.css?v=<?=time();?>">

	<link rel="stylesheet" href="assets/css/theme.css?v=<?=time();?>">
	
	

	

</head>



<body class="theme-lavendar io-lavendar" data-spy="scroll" data-target="#mainnav" data-offset="80">



	<!-- Header --> 

	<header class="site-header is-sticky">

		<!-- Navbar -->

		<div class="navbar navbar-expand-lg is-transparent" id="mainnav">

			<nav class="container">



				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./" id="logo" >

					<img class="logo logo-dark" alt="logo" src="images/ABEERide_logo2.png" srcset="images/ABEERide_logo2.png 2x">

					<img class="logo logo-light" alt="logo" src="images/ABEERide_logo.png" srcset="images/ABEERide_logo.png 2x">

				</a>

				

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">

					<span class="navbar-toggler-icon">

						<span class="ti ti-align-justify"></span>

					</span>

				</button>



				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">

					<ul class="navbar-nav animated remove-animation"" data-animate="fadeInDown" data-delay=".75" style="margin-right: 57%;">

						

						<li class="nav-item"  style="float:left;"><a class="nav-link menu-link" href="#token">Drivers</a></li>

						<li class="nav-item"  style="float:left;"><a class="nav-link menu-link" href="#roadmap">Riders</a></li>

						

					</ul>

					<ul class="navbar-btns animated remove-animation" data-animate="fadeInDown" data-delay=".85">

						<li class="nav-item"><a class="nav-link btn btn-white btn-sm menu-link" href="#" style="margin-right: 80px; display: inline-block;">Become A Driver</a></li>

						

					</ul>

				</div>

			</nav>

		</div>

		<!-- End Navbar -->



		<!-- Banner/Slider -->

		<div id="header" class="banner">

			<div class="banner-rounded-bg">

				<span class="banner-shade-1">

					<span class="banner-shade-2">

						<span class="banner-shade-3"></span>

					</span>

				</span>

			</div>

			<div class="container">

				<div class="banner-content">

					<div class="row text-center">

						<div class="col-lg-12">

							<div class="header-txt">

								<h1 class="animated" data-animate="fadeInUp" data-delay="1.25">ICO Template <span>designed by</span> iO<br class="d-none d-xl-block"> <span>and for</span> blockchain developer <span>or</span> ICO Startups</h1>

								<p class="lead animated" data-animate="fadeInUp" data-delay="1.35">ICO Crypto is the best selling, modern, clean and elegant design, created for ICO<br class="d-none d-xl-block"> development agencies. It’s Include all necessary features that fit for ICOs.</p>

							</div>

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .banner-content  -->

				<div class="row justify-content-center text-center" >
				    
					<div class="col-lg-8">
							<!--<img src="images/verticalplaceholderimage.png" style="padding-top: 55px;" />-->
					</div>
					<div class="col-lg-4">

						

						<div class="gaps"></div>

						<h4 class="animated" data-animate="fadeInUp" data-delay="1.55">Sign Up Here To Use ABEE:</h4>

						<div class="gaps size-0-5x"></div>

						<div class="token-box shadow animated" data-animate="fadeInUp" data-delay="1.65" style="overflow: hidden;">

							<div class="row">

                           

                           <div class="col-md-6>

                               <div class="user-pro-form">

                                   <nav>

                                      <div class="nav nav-tabs justify-content-center" id="nav-tab">

                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#cont-signup">Driver</a>

                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#cont-signin">Rider</a>

                                      </div>

                                    </nav>

                                    <div class="tab-content" id="nav-tabContent">

                                        <div class="tab-pane fade show active" id="cont-signup" role="tabpanel">

                                            <span class="form-heading">Create an account to Join ICO Crypto community and buy ICOX Token.</span>

                                            <form action="#" class="signup-form">

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Name" class="input-border-simple">

                                                    <em class="fas fa-user"></em>

                                                </div>

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Email" class="input-border-simple">

                                                    <em class="fas fa-envelope"></em>

                                                </div>
												
												<div class="input-item">

                                                    <input type="text" placeholder="Your City" class="input-border-simple">

                                                    <em class="fas fa-building"></em>

                                                </div> 

                                                <div class="row gutter-20">

                                                    <div class="col-lg-6">

                                                        <div class="input-item">

                                                            <input type="text" placeholder="Telephone" class="input-border-simple">

                                                            <em class="fas fa-phone"></em>

                                                        </div>

                                                    </div>

                                                    <div class="col-lg-6">

                                                        <div class="input-item">

                                                            <input type="text" placeholder="How did you hear about us" class="input-border-simple">

                                                            <em class="fas fa-info"></em>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="input-item text-left">

                                                    <input type="checkbox" name="aggrement" id="aggrement" class="input-checkbox">

                                                    <label for="aggrement">I confrim that I have read, understand and agree to the <a href="#">Privacy Policy</a> and <a href="#">Terms of Use</a>.</label>

                                                </div>

                                                <div class="d-xl-flex justify-content-between align-items-center">

                                                    <button class="btn btn-sm">Sign Up</button>

                                                    <div class="gaps d-xl-none"></div>

                                                    <span class="d-block">Have an account? <a href="#" class="simple-link">Sign in!</a></span>

                                                </div>

                                            </form>

                                        </div>

                                        <div class="tab-pane fade" id="cont-signin" role="tabpanel">

                                            <span class="form-heading">Create an account to Join ICO Crypto community and buy ICOX Token.</span>

                                            <form action="#" class="signup-form">

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Name" class="input-border-simple">

                                                    <em class="fas fa-user"></em>

                                                </div>

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Email" class="input-border-simple">

                                                    <em class="fas fa-envelope"></em>

                                                </div>
												
												<div class="input-item">

                                                    <input type="text" placeholder="Your City" class="input-border-simple">

                                                    <em class="fas fa-building"></em>

                                                </div> 

                                                <div class="row gutter-20">

                                                    <div class="col-lg-6">

                                                        <div class="input-item">

                                                            <input type="text" placeholder="Telephone" class="input-border-simple">

                                                            <em class="fas fa-phone"></em>

                                                        </div>

                                                    </div>

                                                    <div class="col-lg-6">

                                                        <div class="input-item">

                                                            <input type="text" placeholder="How did you hear about us" class="input-border-simple">

                                                            <em class="fas fa-info"></em>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="input-item text-left">

                                                    <input type="checkbox" name="aggrement" id="aggrement" class="input-checkbox">

                                                    <label for="aggrement">I confrim that I have read, understand and agree to the <a href="#">Privacy Policy</a> and <a href="#">Terms of Use</a>.</label>

                                                </div>

                                                <div class="d-xl-flex justify-content-between align-items-center">

                                                    <button class="btn btn-sm">Sign Up</button>

                                                    <div class="gaps d-xl-none"></div>

                                                    <span class="d-block">Have an account? <a href="#" class="simple-link">Sign in!</a></span>

                                                </div>

                                            </form>
                                        </div>

                                    </div>

                               </div>

                           </div>

                       </div>

						</div><!-- EO token box -->

					</div>

				</div>

			</div><!-- .container  -->

		</div>

	</header>

	<!-- End Header -->






	

	



	

	



	

	

	<!-- Start Section -->

	<div class="section reason-scetion no-pb section-pad section-bg-light" id="why">

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Why Choose us?</h2>

			</div>

			<div class="shadow round-md plr-x4">

				<div class="gaps size-2x"></div>

				<div class="row text-center">

					<div class="col-md-6 res-m-bttm">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".2">

							<img src="images/lavender-icon-a.png" alt="lav">

							<h5>Secured User Data</h5>

							<p>We protect user data and emo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.</p>

						</div>

					</div>

					<div class="col-md-6 res-m-bttm">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".3">

							<img src="images/lavender-icon-b.png" alt="lav">

							<h5>Most Credibility</h5>

							<p>Most authentically sed do eiusmod tempor incididunt ut labore et dolore magna aliqua cillum dolore eu fugiat ut labore et pariatur.</p>

						</div>

					</div>

					<div class="col-md-6 res-m-bttm">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".4">

							<img src="images/lavender-icon-c.png" alt="lav">

							<h5>Flexibility &amp; Easy to Use</h5>

							<p>You can trade &amp; invest toekn oluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere poss, omnis voluptas.</p>

						</div>

					</div>

					<div class="col-md-6">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".5">

							<img src="images/lavender-icon-d.png" alt="lav">

							<h5>Big Data Insights</h5>

							<p>Our machines learning are able tosed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>

						</div>

					</div>

				</div><!-- .row  -->

				<div class="gaps size-2x"></div>

			</div>

		</div><!-- .container  -->

	</div>

	<!-- End Section --> 

	

	

	<!-- Start Section -->

	<div class="section process-scetion section-pad section-bg-light">

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">How ICOX Works?</h2>

			</div>

			<div class="row align-items-center">

				<div class="col-lg-7">

					<div class="slider-nav">

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".2"><em class="ikon ikon-paricle"></em></div>

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".3"><em class="ikon ikon-connect"></em></div>

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".4"><em class="ikon ikon-target"></em></div>

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".5"><em class="ikon ikon-shiled"></em></div>

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".6"><em class="ikon ikon-id-card"></em></div>

					</div>

				</div><!-- .col  -->

				<div class="col-lg-5">

					<div class="slider-pane animated" data-animate="fadeInUp" data-delay=".7">

						<div class="slider-pane-item pane-item-1">

							<h5 class="animate-up delay-5ms">Build blockchain algotrading models.</h5>

							<p class="animate-up delay-6ms">We build the algotrading models tosed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliqu am quaerat voluptatem. Numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat volup  tatem. Ut enim ad minima veniam, quis nostrum exercitation em ullam corporis suscipit laboriosam.</p>

						</div>

						<div class="slider-pane-item pane-item-2">

							<h5 class="animate-up delay-5ms">Connect with others blockchain mod.</h5>

							<p class="animate-up delay-6ms">We build the algotrading models tosed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliqu am quaerat voluptatem. Numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat volup  tatem. Ut enim ad minima veniam, quis nostrum exercitation em ullam corporis suscipit laboriosam.</p>

						</div>

						<div class="slider-pane-item pane-item-3">

							<h5 class="animate-up delay-5ms">Target your goal to reach out.</h5>

							<p class="animate-up delay-6ms">We build the algotrading models tosed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliqu am quaerat voluptatem. Numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat volup  tatem. Ut enim ad minima veniam, quis nostrum exercitation em ullam corporis suscipit laboriosam.</p>

						</div>

						<div class="slider-pane-item pane-item-4">

							<h5 class="animate-up delay-5ms">Protect your blockchain models.</h5>

							<p class="animate-up delay-6ms">We build the algotrading models tosed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliqu am quaerat voluptatem. Numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat volup  tatem. Ut enim ad minima veniam, quis nostrum exercitation em ullam corporis suscipit laboriosam.</p>

						</div>

						<div class="slider-pane-item pane-item-5">

							<h5 class="animate-up delay-5ms">Make transaction easy and with a less fee.</h5>

							<p class="animate-up delay-6ms">We build the algotrading models tosed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliqu am quaerat voluptatem. Numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat volup  tatem. Ut enim ad minima veniam, quis nostrum exercitation em ullam corporis suscipit laboriosam.</p>

						</div>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section --> 

	

	


	

	

	<!-- Start Section -->

	<div class="section features-section section-pad no-pb section-bg-lavendar">

		<div class="shade-wraper">

			<div class="container">

				<div class="shade-left"></div>

				<div class="shade-right"></div>

			</div>

		</div>

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">ICO Wallet Feature</h2>

			</div>

			<div class="row">

				<div class="col-lg-4 col-md-6 col-sm-12 order-lg-0 order-md-0">

					<div class="gaps"></div>

					<div class="features-item left animated" data-animate="fadeInUp" data-delay=".2">

						<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>
						<!--<img src="images/signal.png"  alt="" style="border-radius: 50%; display: inline-block;" />-->

						<h5>Manage your Wallet</h5>

						<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>

					</div>

					<div class="features-item left animated" data-animate="fadeInUp" data-delay=".3">

						<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>

						<h5>Reward &amp; Bonus</h5>

						<p>Eiusmod temvpor incididunt ut labore et dolore magna, omet enim ad minim veniam nostrud exerc itation oeme ullam.</p>

					</div>

				</div>

				<div class="col-lg-4 col-md-12 col-sm-12 order-lg-1 order-md-2 order-last mt-auto text-center">

					<div class="features-photo animated" data-animate="fadeInUp" data-delay=".6">

						<!--<img src="images/mobile-app-a.png" alt="mobile app">-->
						<img src="images/abeeio/Mobile_Screen_v2.png" alt="mobile app">

					</div>

				</div>

				<div class="col-lg-4 col-md-6 col-sm-12 order-lg-2 order-md-1">

					<div class="gaps"></div>

					<div class="features-item animated" data-animate="fadeInUp" data-delay=".4">

						<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>

						<h5>Global Tradding</h5>

						<p>Eiusmod tempor incid idunt ut labore et dolore magna ad minim veni ioam nostrud exerc itation oeme ullam.</p>

					</div>

					<div class="features-item animated" data-animate="fadeInUp" data-delay=".5">

						<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>

						<h5>Stay with Friend </h5>

						<p>Yoing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ad minim veniam nostrud exerc itation</p>

					</div>

				</div>

			</div>

		</div><!-- .container  -->

	</div>

	<!-- End Section -->

	

	


	

	




 

	<!-- Start Section -->

	<div class="section media-scetion no-pb section-pad section-bg-light" id="media">

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Media Says About Us</h2>

			</div>

			<div class="has-carousel animated" data-animate="fadeInUp" data-delay=".2" data-items="3" data-dots="true" data-navs="false">

				<div class="media-box">

					<img src="images/partner-md-c.png" alt="media">

					<h5 class="media-heading"><a href="#">ICOX coin build with modern blockchain technology</a></h5>

					<p>Energy dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>

				</div>

				<div class="media-box">

					<img src="images/partner-md-b.png" alt="media">

					<h5 class="media-heading"><a href="#">ICOX coin is more powerful cryptocurrencies</a></h5>

					<p>Digital coin untsed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim </p>

				</div>

				<div class="media-box">

					<img src="images/partner-md-a.png" alt="media">

					<h5 class="media-heading"><a href="#">How &amp; Where To Market Your ICO Startup Business</a></h5>

					<p>Business idea sunt in minim veniam, quis nostrud exercitation ullamco laboris ea commodo conseq.</p>

				</div>

				<div class="media-box">

					<img src="images/partner-md-d.png" alt="media">

					<h5 class="media-heading"><a href="#">ICOX coin build with modern blockchain technology</a></h5>

					<p>Energy dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>

				</div>

				<div class="media-box">

					<img src="images/partner-md-e.png" alt="media">

					<h5 class="media-heading"><a href="#">ICOX coin is more powerful cryptocurrencies</a></h5>

					<p>Digital coin untsed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim </p>

				</div>

			</div>

		</div>

	</div>

	<!-- End Section -->

	
	
	
	

	

	<!-- Start Section -->

	<div class="section subscribe-scetion section-pad section-bg-light" id="subscribe">

		<div class="container">

			<div class="row  justify-content-center text-center">

				<div class="col-lg-6 res-l-bttm">

					<div class="subscribe-rounded">

						<h5 class="animated" data-animate="fadeInUp" data-delay=".1">Don’t miss out, Be the first to know</h5>

						<p class="animated" data-animate="fadeInUp" data-delay=".2">Get notified about the details of the Token Sale in May, as well as other important development update.</p>

						<form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form animated" data-animate="fadeInUp" data-delay=".3">

							<input type="text" name="youremail" class="input-round required email" placeholder="Enter your email" >

							<input type="text" class="d-none" name="form-anti-honeypot" value="">

							<button type="submit" class="btn btn-plane">Let me know</button>

							<div class="subscribe-results"></div>

						</form>

						<div class="gaps"></div>

						<a href="#" class="btn btn-simple animated" data-animate="fadeInUp" data-delay=".4"> <em class="fa fa-paper-plane"></em> Join us on Telegram</a>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section -->

	

	<!-- Start Section -->

	<div class="section footer-scetion footer-lavendar">

		<div class="social-overlap">

			<div class="container">

				<div class="row justify-content-center">

					<div class="col-md-10">

						<ul class="social-bar animated" data-animate="fadeInUp" data-delay=".1">

							<li>Follow us</li>

							<li><a href="#"><em class="fab fa-facebook-f"></em></a></li>

							<li><a href="#"><em class="fab fa-twitter"></em></a></li>

							<li><a href="#"><em class="fab fa-youtube"></em></a></li>

							<li><a href="#"><em class="fab fa-github"></em></a></li>

							<li><a href="#"><em class="fab fa-bitcoin"></em></a></li>

							<li><a href="#"><em class="fab fa-medium-m"></em></a></li>

						</ul>

					</div>

				</div>

			</div>

		</div>

		<div class="gaps size-4x"></div>

		<div class="gaps size-2x d-none d-sm-block"></div>

		<div class="container">

			<div class="row justify-content-center">

				<div class="col-sm-12 col-lg-3 res-m-bttm">

					<a class="footer-brand animated" data-animate="fadeInUp" data-delay=".2" href="./">

						<img class="logo logo-light" alt="logo" src="images/ABEERide_logo.png" srcset="images/ABEERide_logo.png 2x">

					</a>

				</div><!-- .col  -->

				<div class="col-sm-4 col-lg-2 res-l-bttm">

					<ul class="link-widget one-column animated" data-animate="fadeInUp" data-delay=".3">

						<li><a href="#token" class="menu-link">Tokens Sales</a></li>

						<li><a href="#roadmap" class="menu-link">Roadmap</a></li>

						<li><a href="#about" class="menu-link">What is ICOX</a></li>

						<li><a href="#why" class="menu-link">why / How ?</a></li>

						<li><a href="#media" class="menu-link">Media</a></li>

					</ul>

				</div><!-- .col  -->

				<div class="col-sm-4 col-lg-2 res-l-bttm">

					<ul class="link-widget one-column animated" data-animate="fadeInUp" data-delay=".4">

						<li><a href="#team" class="menu-link">Teams</a></li>

						<li><a href="#about" class="menu-link">About</a></li>

						<li><a href="#contact" class="menu-link">Contact</a></li>

						<li><a href="#faq" class="menu-link">Faqs</a></li>

						<li><a href="#subscribe" class="menu-link">Join Us</a></li>

					</ul>

				</div><!-- .col  -->

				<div class="col-sm-4 col-lg-3">

					<ul class="address-widget animated" data-animate="fadeInUp" data-delay=".5">

						<li>1330 Cooks Mine Road <br> Twin Lakes, Mexico - 86515</li>

						<li>+505-735-0177</li>

						<li>hello@icocrypto.nio</li>

					</ul>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .container  -->

		

		<hr class="hr-line"><!-- .hr-line  -->

		

		<div class="container">

			<div class="row">

				<div class="col-md-7">

					<span class="copyright-text">

						Copyright &copy; 2018, ICO Crypto. Template Made By <a href="http://softnio.com" target="_blank">Softnio</a> &amp; Handcrafted by iO.

					</span>

				</div><!-- .col  -->

				<div class="col-md-5 text-right mobile-left">

					<ul class="footer-links">

						<li><a href="#">Privacy Policy</a></li>

						<li><a href="#">Terms &amp; Conditions</a></li>

					</ul>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .container  -->

		<div class="gaps size-2x"></div>

	</div>

	<!-- End Section -->



	<!-- Preloader !remove please if you do not want -->

	<div id="preloader">

		<div id="loader"></div>

		<div class="loader-section loader-top"></div>

   		<div class="loader-section loader-bottom"></div>

	</div>

	<!-- Preloader End -->



	<!-- JavaScript (include all script here) -->

	<script src="assets/js/jquery.bundle.js?v=<?=time();?>"></script>

	<script src="assets/js/script.js?v=<?=time();?>">></script>



</body>

</html>

