<?php

if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){

    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    header('HTTP/1.1 301 Moved Permanently');

    header('Location: ' . $redirect);

    exit();

}

?>

<!DOCTYPE html>



<html lang="zxx" class="js">



<head>











	<meta charset="utf-8">



	<meta name="author" content="ABEE Foundation">



	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



	<meta name="description" content="ABEE Foundation">



	<!-- Fav Icon  -->



	<link rel="shortcut icon" href="images/abee3.png">



	<!-- Site Title  -->



	<title>ABEE Foundation</title>



	<!-- Vendor Bundle CSS -->



	<link rel="stylesheet" href="assets/css/vendor.bundle.css?v=<?=time();?>">



	<!-- Custom styles for this template -->



	<link rel="stylesheet" href="assets/css/style.css?v=<?=time();?>">



	<link rel="stylesheet" href="assets/css/theme-java.css?v=<?=time();?>">







</head>







<body class="theme-dark io-azure io-azure-pro" data-spy="scroll" data-target="#mainnav" data-offset="80">







	<!-- Header -->



	<header class="site-header is-sticky">



		<!-- Navbar -->



		<div class="navbar navbar-expand-lg is-transparent" id="mainnav">



			<nav class="container">



				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./">



					<img class="logo logo-dark" alt="logo" src="images/ABEElogo.png" srcset="images/ABEElogo.png 2x">



					<img class="logo logo-light" alt="logo" src="images/ABEElogo.png" srcset="images/ABEElogo.png 2x">



				</a>







				<!--<div class="language-switcher animated" data-animate="fadeInDown" data-delay=".75">



					<a href="#" data-toggle="dropdown">EN</a>



					<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">



						<a class="dropdown-item" href="#">FR</a>



						<a class="dropdown-item" href="#">CH</a>



						<a class="dropdown-item" href="#">BR</a>



					</div>



				</div>-->







				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">



					<span class="navbar-toggler-icon">



						<span class="ti ti-align-justify"></span>



					</span>



				</button>







				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">



					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".9">



						<li class="nav-item"><a class="nav-link menu-link" href="#intro">ABEE<span class="sr-only">(current)</span></a></li>



						<li class="nav-item"><a class="nav-link menu-link" href="#tokenSale">TGE</a></li>



						<li class="nav-item"><a class="nav-link menu-link" href="#roadmap">Roadmap</a></li>



						<li class="nav-item"><a class="nav-link menu-link" href="#apps">App</a></li>



						<li class="nav-item"><a class="nav-link menu-link" href="#team">Team</a></li>



						<li class="nav-item"><a class="nav-link menu-link" href="#faq">Faq</a></li>



					</ul>



					<ul class="navbar-nav navbar-btns animated remove-animation" data-animate="fadeInDown" data-delay="1.15">



						<li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" target= "_blank" href="https://token.abeefoundation.org">AXB Tokens</a></li>



                        <li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" target= "_blank" href="https://docs.google.com/forms/d/1JauzuDzlY9u9pv-Tvb40CN6WKGdXvKIRUKjGNdqzyZc/viewform?edit_requested=true">Pre-TGE / Whitelist</a></li>



					</ul>



				</div>



			</nav>



		</div>



		<!-- End Navbar -->







		<!-- Banner/Slider -->



		<div id="header" class="banner banner-full d-flex align-items-center">



			<div class="container">



				<div class="banner-content">



					<div class="row align-items-center mobile-center">



						<div class="col-lg-6 col-md-12 order-lg-first">



							<div class="header-txt">



								<h1 class="animated" data-animate="fadeInUp" data-delay="1.55"><br class="d-none d-xl-block"> <br class="d-none d-xl-block">The ABEE Foundation</h1>



								<p class="lead animated" data-animate="fadeInUp" data-delay="1.65">Funding the good-faith sharing economy</p>



								<ul class="btns animated" data-animate="fadeInUp" data-delay="1.75">



									<li><a href="assets/documents/AF whitepaper iv.pdf" class="btn btn-alt">Whitepaper</a></li>



									<li><a target= "_blank" href="https://token.abeefoundation.org" class="btn btn-alt">AXB Tokens</a></li>



								</ul>



							</div>



						</div><!-- .col  -->



						<div class="col-lg-6 col-md-12 order-first res-m-bttm-lg">



                            <div class="video-graph res-m-btm animated" data-animate="fadeInUp" data-delay=".1">



						<img src="images/gfx-white_2x.png" alt="graph" style="max-width:120%; padding-right: 70px;" >



						<a href="https://vimeo.com/292266289" class="play-btn-alt video-play">



							<em class="fa fa-play"></em>



							<!--<span>How it work</span>-->



						</a>



					</div>







							<!--<div class="header-image-alt animated" data-animate="fadeInRight" data-delay="1.25">



								<img src="images/header-image-blue.png" alt="header">



							</div>-->



						</div><!-- .col  -->



					</div><!-- .row  -->



				</div><!-- .banner-content  -->



			</div><!-- .container  -->



		</div>



		<!-- End Banner/Slider -->



		<div class="header-partners">



			<div class="container">



				<div class="row align-items-center">



					<ul class="partner-list-s2 d-flex flex-wrap align-items-center">



						<li class="animated" data-animate="fadeInUp" data-delay="1.85">ABEE Foundation & ABEE Rideshare's <br> Partners</li>



						<li class="animated" data-animate="fadeInUp" data-delay="1.9"><a href="#"><img src="assets/images/dash_new.png" width="50%" alt="partner"></a></li>



						<li class="animated" data-animate="fadeInUp" data-delay="1.95"><a href="#"><img src="assets/images/chain_new.png" alt="partner"></a></li>



						<li class="animated" data-animate="fadeInUp" data-delay="2"><a href="#"><img src="assets/images/semada.png" alt="partner"></a></li>



						<!--<li class="animated" data-animate="fadeInUp" data-delay="2.05"><a href="#"><img src="images/mikevision.jpg" alt="partner"></a></li>



						<!--<li class="animated" data-animate="fadeInUp" data-delay="2.1"><a href="#"><img src="images/partner-xs-light-e.png" alt="partner"></a></li>-->



					</ul>



				</div><!-- .row  -->



			</div><!-- .container  -->



		</div><!-- .header-partners  -->



	</header>



	<!-- End Header -->







	<!-- Start Section -->



	<div class="section section-pad section-bg section-pro nopb into-section" id="intro">



		<div class="container">



			<div class="row align-items-center">



				<div class="col-md-5 offset-md-1">



					<div class="video-graph res-m-btm animated" data-animate="fadeInUp" data-delay=".1">



						<img src="images/ABEEcircle.png" alt="graph">



						<!--<a href="https://www.youtube.com/watch?v=SSo_EIwHSd4" class="play-btn-alt video-play">



							<em class="fa fa-play"></em>



							<span>How it work</span>



						</a>-->



					</div>



				</div><!-- .col  -->



				<div class="col-md-6 order-md-first order-last">



					<div class="text-block">



						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Why ABEE Rideshare?</h6>



						<h2 class="animated" data-animate="fadeInUp" data-delay=".1">Setting the stage for Rideshare 3.0</h2>



						<p class="lead animated" data-animate="fadeInUp" data-delay=".2">The disruption of a $36 billion industry set to surge 800% by 2030.</p>



						<p class="animated" data-animate="fadeInUp" data-delay=".3">The ABEE Foundation is funding ABEE Rideshare because of its quintessential balance of centralized and decentralized functions. After months of research and interviewing we have found the perfect team with the right solutions to usher in the next era of rideshare. </p>



						<p class="animated" data-animate="fadeInUp" data-delay=".4">ABEE Rideshare has created a suite of dynamic features that users want and competitors have failed to deliver on.  Rideshare companies have paved the way, providing important insight so ABEE can avoid their mistakes and expand on the industry without the accumulation of enormous debt, liabilities and unnecessary overhead.</p>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



		</div><!-- .conatiner  -->



	</div>



	<!-- Start Section -->











	<!-- Start Section -->



	<div class="section section-pad section-bg section-pro">



		<div class="container">



			<div class="row text-center">



				<div class="col-lg-8 offset-lg-2">



					<div class="section-head-s2">



						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0"></h6>



						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Competitive Advantage</h2>



						<p class="animated" data-animate="fadeInUp" data-delay=".2">We are proud to stand behind ABEE, with good faith business practices and a strategic balance of services make the company stand out from the rest.</p>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



			<div class="row text-center">



				<div class="col-md-4">



					<div class="features-box animated" data-animate="fadeInUp" data-delay=".2">



						<img src="images/Icon_1.png" alt="features">



						<h5>Superior User Experience</h5>



						<p>ABEE Rideshare takes 60% less than major competitors, while giving users added benefits and control.</p>



					</div>



				</div><!-- .col  -->



				<div class="col-md-4">



					<div class="features-box animated" data-animate="fadeInUp" data-delay=".3">



						<img src="images/Icon_3.png" alt="features">



						<h5>Good Faith Business</h5>



						<p>Structuring a business from the ground up that is catering to users� needs in a mutually beneficial and sustainable manner.</p>



					</div>



				</div><!-- .col  -->



				<div class="col-md-4">



					<div class="features-box animated" data-animate="fadeInUp" data-delay=".4">



						<img src="images/Icon_2.png" alt="features">



						<h5>Crypto To The Masses</h5>



						<p>ABEE Rideshare found a subtle and impactful way to integrate decentralized blockchain services and crypto currency payments into a flourishing industry.</p>





					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



			<div class="gaps size-2x d-none d-md-block"></div>



			<div class="row text-center justify-content-center">



				<div class="col-md-12">



					<ul class="btns">



						<li class="animated" data-animate="fadeInUp" data-delay=".0"><a href="assets/documents/AF whitepaper iv.pdf" class="btn btn-alt">Download Whitepaper</a></li>



						<li class="animated" data-animate="fadeInUp" data-delay=".05"><a href="https://docs.google.com/forms/d/1JauzuDzlY9u9pv-Tvb40CN6WKGdXvKIRUKjGNdqzyZc/viewform?edit_requested=true" class="btn btn-alt">Apply To Get Whitelisted</a></li>



					</ul>



					<div class="gaps size-1x d-none d-md-block"></div>



					<div class="animated" data-animate="fadeInUp" data-delay=".1">



						<a href="https://t.me/joinchat/FqphaE8-dbFUcHR76ELcsA" class="btn btn-simple"> <em class="fa fa-paper-plane"></em> Join us on Telegram</a>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



		</div><!-- .conatiner  -->



		<div class="mask-ov-right mask-ov-s2"></div><!-- .mask overlay -->



	</div>



	<!-- Start Section -->











	<!-- Start Section -->



	<div class="section section-pad section-bg token-section section-gradiant" id="tokenSale">



		<div class="container">



			<div class="row text-center">



				<div class="col-lg-6 offset-lg-3">



					<div class="section-head-s2">



						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">AXB Tokens</h6>



						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Pre &amp; Main TGE</h2>



						<p class="animated" data-animate="fadeInUp" data-delay=".2">Participate in the ABEE Foundation's TGE. The AXB token is on the Qtum blockchain. It is safest to store QRC20s with the Qtum Electrum wallet with Trezor or in the Qtum Web Wallet with Ledger Nano S.</p>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



			<div class="row align-items-center">



				<div class="col-lg-7 res-m-bttm">



					<div class="row event-info">



						<div class="col-sm-6">



							<div class="event-single-info animated" data-animate="fadeInUp" data-delay="0">



								<h6>Start</h6>



								<p>Nov 15, 2018 (6:00AM GMT)</p>



							</div>



						</div><!-- .col  -->



						<div class="col-sm-6">



							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".1">



								<h6>AXB Tokens Up For Contribution</h6>



								<p>1,890,000,000 AXB (63%)</p>



							</div>



						</div><!-- .col  -->



						<div class="col-sm-6">



							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".2">



								<h6>End</h6>



								<p>2 months once soft cap is reached or hard cap stop.</p>



							</div>



						</div><!-- .col  -->



						<div class="col-sm-6">



							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".3">



								<h6>Tokens exchange rate</h6>



								<p>1 QTUM = 1040 AXB, 1 BTC = 799680 AXB</p>



							</div>



						</div><!-- .col  -->



						<div class="col-sm-6">



							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".4">



								<h6>Acceptable currencies</h6>



								<p>QTUM, BTC, DASH, ETH</p>



							</div>



						</div><!-- .col  -->



						<div class="col-sm-6">



							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".5">



								<h6>Minimal transaction amount</h6>



								<p>$100 USD equivalent</p>



							</div>



						</div><!-- .col  -->



					</div><!-- .row  -->



				</div><!-- .col  -->



				<div class="col-lg-5">



					<div class="countdown-box text-center animated" data-animate="fadeInUp" data-delay=".3">



						<h6>Pre-TGE will start in</h6>



						<div class="token-countdown d-flex align-content-stretch" data-date="2018/11/15"></div>



						<a href="https://token.abeefoundation.org" class="btn btn-alt btn-sm">Contribute now</a>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->







			<div class="gaps size-3x"></div><div class="gaps size-3x d-none d-md-block"></div><!-- .gaps  -->







			<div class="row text-center">



				<div class="col-md-6">



					<div class="single-chart light res-m-btm">



						<h3 class="sub-heading">TGE Fund Allocation</h3>



						<div class="animated" data-animate="fadeInUp" data-delay=".0">



							<img src="images/black chart.png" alt="chart">



						</div>



					</div>



				</div><!-- .col  -->







				<div class="col-md-6">



					<div class="single-chart light">



						<h3 class="sub-heading">AXB Token Allocation</h3>



						<div class="animated" data-animate="fadeInUp" data-delay=".1">



							<img src="images/white chart.png" alt="chart">



						</div>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->







		</div><!-- .container  -->



	</div>



	<!-- Start Section -->





	<!-- Start Section -->



	<div class="section section-pad section-bg" id="roadmap" style="background-color: #10122d;">

		<div class="container">

		    <div class="row">

		        <div class="col-lg-6">

		            <div class="section-head-s6">

                        <h6 class="heading-sm-s2 animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">TIMELINE</h6>

                        <h2 class="section-title-s6 animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s; color: #1641b5;">Road Map</h2>

                        <p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".3" style="visibility: visible; animation-delay: 0.3s;"></p>

                    </div>

                    <div class="gaps size-1x"></div>

                    <div class="gaps size-2x d-none d-md-block"></div>

		        </div>

		    </div>

            <div class="timeline-row timeline-row-odd timeline-row-done animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="visibility: visible; animation-delay: 0.4s;">

               <div class="row no-gutters text-left text-lg-center justify-content-center">

                   <div class="col-lg">

                       <div class="timeline-item timeline-done">

                           <span class="timeline-date">2017 Q1</span>

                           <h6 class="timeline-title">Inception</h6>

                           <ul class="timeline-info">

                               <li>Concept generation</li>

                               <li>Building team and network</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item timeline-done">

                           <span class="timeline-date">2017 Q2</span>

                           <h6 class="timeline-title">Analysis</h6>

                           <ul class="timeline-info">

                               <li>Market analysis and research</li>

                               <li>Strategic plan</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item timeline-done">

                           <span class="timeline-date">2017 Q3</span>

                           <h6 class="timeline-title">Design</h6>

                           <ul class="timeline-info">

                               <li>Platform design</li>

                               <li>Building MVP</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item timeline-done">

                           <span class="timeline-date">2017 Q4</span>

                           <h6 class="timeline-title">Development</h6>

                           <ul class="timeline-info">

                               <li>Feature integration</li>

                               <li>Front end development begins</li>

                           </ul>

                       </div>

                   </div>

               </div>

           </div>

           <div class="timeline-row timeline-row-even animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="visibility: visible; animation-delay: 0.4s;">

               <div class="row no-gutters text-left text-lg-center justify-content-center flex-row-reverse">

                   <div class="col-lg">

                       <div class="timeline-item timeline-done">

                           <span class="timeline-date">2018 Q1</span>

                           <h6 class="timeline-title">Development</h6>

                           <ul class="timeline-info">

                               <li>Back end development begins</li>

                               <li>Beginning of marketing strategy</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item timeline-done">

                           <span class="timeline-date">2018 Q2</span>

                           <h6 class="timeline-title">App</h6>

                           <ul class="timeline-info">

                               <li>ABEE app launched in App Store/Play Store</li>

                               <li>Whitepaper completion</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item timeline-current">

                           <span class="timeline-date">2018 Q3</span>

                           <h6 class="timeline-title">Integration</h6>

                           <ul class="timeline-info">

                               <li>Dash payment integration</li>

                               <li>On Trip Ads platform integration</li>

                               <li>App stress tests</li>

                           </ul>

                       </div>

                   </div>

               </div>

           </div>

           <div class="timeline-row timeline-row-odd timeline-row-last mb--x5 animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="visibility: visible; animation-delay: 0.4s;">

               <div class="row no-gutters text-left text-lg-center justify-content-center">

                   <div class="col-lg">

                       <div class="timeline-item">

                           <span class="timeline-date">2018 Q4</span>

                           <h6 class="timeline-title">TGE</h6>

                           <ul class="timeline-info">

                               <li>TNC license and insurance for ABEE Rideshare</li>

                               <li>Small ride demos</li>

                               <li>ABEE Foundation TGE begins</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item">

                           <span class="timeline-date">2019 Q1</span>

                           <h6 class="timeline-title">Launch</h6>

                           <ul class="timeline-info">

                               <li>Launch of ABEE Rideshare platform</li>

                               <li>ABEE Foundation begins think tank role</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item">

                           <span class="timeline-date">2019 Q2</span>

                           <h6 class="timeline-title">Marketing</h6>

                           <ul class="timeline-info">

                               <li>ABEE Rideshare begins major marketing campaign</li>

                               <li>ABEE Foundation begins ABEE VC</li>

                           </ul>

                       </div>

                   </div>

                   <div class="col-lg">

                       <div class="timeline-item">

                           <span class="timeline-date">2019 Q3</span>

                           <h6 class="timeline-title">Expansion</h6>

                           <ul class="timeline-info">

                               <li>ABEE Rideshare expands to second city</li>

                               <li>First applicants from ABEE VC to be assessed </li>

                           </ul>

                       </div>

                   </div>

               </div>

           </div>

           <div class="gaps size-2x d-lg-none"></div>

        </div>

    </div>







	<!-- Start Section -->











	<!-- Start Section -->



	<div class="section section-pad section-bg-alt section-pro nopb" id="apps">



		<div class="container">



			<div class="row text-center">



				<div class="col-lg-6 offset-lg-3">



					<div class="section-head-s2">



						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">App</h6>



						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">ABEE Rideshare App</h2>



						<p class="animated" data-animate="fadeInUp" data-delay=".2">The ABEE Rideshare app has the feel of the prominent rideshare companies, but with features that the industry has yet to experience.</p>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->







			<div class="row align-items-center">



				<div class="col-md-6 res-m-btm">



					<div class="text-block">



						<p class="animated" data-animate="fadeInUp" data-delay="0">The ABEE Rideshare app provides a suite of features that enables users a more rich and dynamic experience, increasing user controls and accessibilities. </p>



						<ul class="animated" data-animate="fadeInUp" data-delay=".1">



							<li>Earn free or discounted rides every trip with On Trip Ads</li>



							<li>Drivers can upgrade to a Super Driver and give passengers discounts</li>



							<li>Dash payments for P2P transactions</li>



							<li>All platform users can vote on desired upgrades in app</li>



							<li>Scan your drivers' vehicle QR code for instant ride requests</li>



						</ul>



						<ul class="btns">



							<li class="animated" data-animate="fadeInUp" data-delay="0"><a href="https://play.google.com/store/apps/details?id=com.abee.app" class="btn btn-sm">GET THE APP NOW</a></li>



							<li class="animated" data-animate="fadeInUp" data-delay=".2">



								<a href="https://itunes.apple.com/us/app/abee/id1319311951?mt=8"><em class="fab fa-apple"></em></a>



								<!--<a href="#"><em class="fab fa-android"></em></a>-->



							</li>



						</ul>



					</div>



				</div><!-- .col  -->



				<div class="col-md-6 mt-auto ">



					<div class="animated" data-animate="fadeInUp" data-delay="0" style="padding-left: 35px; padding-bottom: 35px;">



						<img src="images/app-screen-v2.png" alt="graph">



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



		</div><!-- .container  -->



	</div>



	<!-- Start Section -->









<!-- Start Section -->

	<div class="section section-pad section-bg-alt section-fix" id="team">

	    <!--<div class="background-shape bs-right"></div>-->

		<div class="container">

			<div class="row">

				<div class="col-lg-6">

				    <div class="section-head-s6">

                        <h6 class="heading-sm-s2 animated" data-animate="fadeInUp" data-delay=".1">Meet The Team</h6>

                        <h2 class="section-title-s6 animated" data-animate="fadeInUp" data-delay=".1">ABEE Foundation Team</h2>

                       <!--<p class="animated" data-animate="fadeInUp" data-delay=".3">The ICO Crypto Team combines a passion for esports, industry experise & proven record in finance, development, marketing &amp; licensing.</p>-->

                    </div>

                    <div class="gaps size-2x"></div>

				</div>

			</div>

			<div class="row text-center">

				<div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".1">

						<div class="team-circle-img"><img src="images/steve.png" alt="team"></div>

						<h5>Steve Zedski</h5>

						<span>Head of ABEE Foundation</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

				<div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".2">

						<div class="team-circle-img"><img src="images/adam4.png" alt="team"></div>

						<h5>Adam Tallamy</h5>

						<span>Board Member</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

				<div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-circle-img"><img src="images/egg.jpg" alt="team"></div>

						<h5>Chris</h5>

						<span>Board Member</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".4">

						<div class="team-circle-img"><img src="images/charles2.png" alt="team"></div>

						<h5>Charles Gonzales</h5>

						<span>TGE Advisor</span>

						<p>Charles has been a Software Engineer for the Komodo platform and is Founder and CEO of Chainzilla.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>



			</div><!-- .row  -->

			<div class="row">

				<div class="col-md-12">

					<div class="gaps size-2x"></div>

					<h2 class="section-title-s6 animated" data-animate="fadeInUp" data-delay=".2">ABEE Rideshare Team</h2>

					<div class="gaps size-2x"></div>

				</div>

			</div>

			<div class="row">

				<div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".1">

						<div class="team-circle-img"><img src="images/gil3.png" alt="team"></div>

						<h5>Gilbert Brown</h5>

						<span>CEO</span>

						<p>Founder of Alley Innovate and a cryptocurrency investor and advisor since 2014.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

				<div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".2">

						<div class="team-circle-img"><img src="images/mukunda.png" alt="team"></div>

						<h5>Mukunda Ramac</h5>

						<span>CTO</span>

						<p>With over 10 years of professional experience, Mukunda has served as Software Engineer for Google, Qualcomm, SAP Labs, &amp; Amazon</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href="https://www.linkedin.com/in/mukunda1/"><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-circle-img"><img src="images/joe2.png" alt="team"></div>

						<h5>Joe Calder</h5>

						<span>CBO</span>

						<p>Joe has over 10 years experience as Senior Program Manager, Board Advisor, and Chief Business Officer at Paypal, Visa, Pearson, Google, &amp; McAfee.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href="https://www.linkedin.com/in/joecalder/"><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".4">

						<div class="team-circle-img"><img src="images/josh.png" alt="team"></div>

						<h5>Josh McKenzie</h5>

						<span>CMO</span>

						<p>Josh has served as Chief Marketing Officer and Director of Business Development for Echobox Audio, MikeVision Tech, and Innovation &amp; Tech Today.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href="https://www.linkedin.com/in/josh-thomas-93309a130/"><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".5">

						<div class="team-circle-img"><img src="images/wulf.png" alt="team"></div>

						<h5>Wulf Kaal</h5>

						<span>DLT Integration Advisor</span>

						<p>A leading expert in Crypto Economics, Wulf is the Founder and CEO of Semada, Professor of Law at University of St. Thomas and Ex Goldman Sachs, Cravath, Swaine &amp; Moore.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href="https://www.linkedin.com/in/wulf-kaal-6904a65b/"><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".6">

						<div class="team-circle-img"><img src="images/cole.png" alt="team"></div>

						<h5>Cole Conway</h5>

						<span>CCO</span>

						<p>Cole has been Media Marketing Coordinator for Innovation and Tech Today.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href="https://www.linkedin.com/in/cole-conway-372967a4/"><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".7">

						<div class="team-circle-img"><img src="images/egg.jpg" alt="team"></div>

						<h5>Ryan Taylor</h5>

						<span>ABEE Rideshare Advisor</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href="https://www.linkedin.com/in/willsizemore/"><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".8">

						<div class="team-circle-img"><img src="images/egg.jpg" alt="team"></div>

						<h5>Maria</h5>

						<span>Intellectual Property Counsel</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay=".9">

						<div class="team-circle-img"><img src="images/judy.png" alt="team"></div>

						<h5>Yuriy Sherayzen</h5>

						<span>Software Engineer</span>

						<p>With 15 years of development experience, Yuriy is Founder &amp; CEO of Level X Capital and Eisen Learning, Inc., CEO at DAppMind</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay="1">

						<div class="team-circle-img"><img src="images/egg.jpg" alt="team"></div>

						<h5>xx</h5>

						<span>Board Advisor</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

                <div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay="1.1">

						<div class="team-circle-img"><img src="images/egg.jpg" alt="team"></div>

						<h5>sheri</h5>

						<span>Board Advisor</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

				<div class="col-lg-3 col-sm-6">

					<div class="team-circle-des animated" data-animate="fadeInUp" data-delay="1.2">

						<div class="team-circle-img"><img src="images/egg.jpg" alt="team"></div>

						<h5>xx</h5>

						<span>Board Advisor</span>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incide.</p>

						<ul class="team-circle-social">

							<!--<li><a href=""><em class="fab fa-facebook-f"></em></a></li>-->

							<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

							<!--<li><a href=""><em class="fab fa-twitter"></em></a></li>-->

						</ul>

					</div>

				</div>

			</div><!-- .row  -->

		</div>

	</div>

	<!-- End Section -->









<!-- Start Section -->



	<div class="section section-pad faq-section section-pro no-pb section-bg" id="faq">



		<div class="container">



			<div class="row text-center">



				<div class="col-lg-6 offset-lg-3">



					<div class="section-head-s2">



						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Faqs</h6>



						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Frequently Asked Questions</h2>



						<p class="animated" data-animate="fadeInUp" data-delay=".2"></p>



					</div>



				</div><!-- .col  -->



			</div><!-- .row  -->



			<div class="row">



				<div class="col-md-12">



					<div class="tab-custom tab-custom-s2">



						<!-- Nav tabs -->



						<ul class="nav nav-tabs text-center animated" data-animate="fadeInUp" data-delay=".15">



							<li class="nav-item">



								<a class="nav-link active" data-toggle="tab" href="#tab-1">General</a>



							</li>



							<li class="nav-item">



								<a class="nav-link" data-toggle="tab" href="#tab-2">Pre-TGE &amp; TGE</a>



							</li>



							<li class="nav-item">



								<a class="nav-link" data-toggle="tab" href="#tab-3">Tokens</a>



							</li>



							<!--<li class="nav-item">



								<a class="nav-link" data-toggle="tab" href="#tab-4">CLIENT</a>



							</li>



							<li class="nav-item">



								<a class="nav-link" data-toggle="tab" href="#tab-5">Legal</a>



							</li>-->



						</ul>



						<!-- Tab panes -->



						<div class="tab-content">



							<div class="tab-pane fade show active" id="tab-1">



								<div class="row">



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">



											<h5>What is the ABEE Foundation and how is it related to ABEE Rideshare?</h5>



											<p>The ABEE Foundation is a non-US entity established in Saint Kitts &amp; Nevis for a seamless international reach. ABEE Rideshare is the first among many companies that will be using the ABEE name and will be receiving equity-free funding from the A.F.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">



											<h5>Why is the ABEE Foundation giving equity free funding?</h5>



											<p>The ABEE Foundation is a philanthropic enterprise. We act as a think tank and funding mechanism for ABEE companies worldwide. We provide funding to teams that are hand selected in order to grow the good faith mission of providing the most ethical and balanced platforms worldwide.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">



											<h5>How will the ABEE Foundation be able to keep giving equity free capital?</h5>



											<p>Although the ABEE Foundation does give equity-free capital to ABEE companies, the ABEE Foundation does gain equity in other hand selected and innovative companies through ABEE VC. ABEE VC encourages new users to switch over to ABEE platforms as they gain the ability to submit their ideas into a VC seed funding round. The ABEE Foundation will help fund individuals' dreams worldwide and will gain equity in these companies, ultimately growing a rich and diverse portfolio for the ABEE Foundation.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">



											<h5>How can I contribute to the ABEE Foundation?</h5>



											<p>Contributors can participate in the Pre and Main TGE by clicking "AXB Tokens" in the top right of this site or visiting https://token.abeefoundation.org. The TGE will begin with the pre round, followed by 3 rounds for a total of 63% of all AXB token supply. </p>



										</div>



									</div><!-- .col  -->



								</div><!-- .row  -->



							</div><!-- End tab-pane -->









							<div class="tab-pane fade" id="tab-2">



								<div class="row">



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">



											<h5>Is there a soft and hard cap?</h5>



											<p>The soft cap will be $2,550,000 and the hard cap will be $30,000,000



                                        </div>



									</div><!-- .col  -->







									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">



											<h5>Can anyone participate? Is there any KYC process?</h5>



											<p>Chinese and US citizens are not able to participate. KYC will be required once the TGE has concluded.</p>



										</div>



									</div><!-- .col  -->







									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">



											<h5>When will I be able to see my token balance?</h5>



											<p>Your AXB balance will be viewable on your personal account at token.abeefoundation.org once the transaction is complete. At the conclusion of the TGE your AXB balance will be sent to the QTUM address you specified on your token.abeefoundation.org profile.</p>



										</div>



									</div><!-- .col  -->











									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">



											<h5>I do not have any experience with QRC20s. Where can I safely store my AXB Tokens?</h5>



											<p>There are a variety of Qtum wallets available, please see https://qtumeco.io/wallet/. With the Qtum Electrum wallet, you can send and receive QRC-20s with your Trezor. The Qtum Web Wallet is similar to MyEtherWallet which can connect to a Ledger Nano S.</p>



										</div>



									</div><!-- .col  -->







								</div><!-- .row  -->



							</div><!-- End tab-pane -->











							<div class="tab-pane fade" id="tab-3">



								<div class="row">



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">



											<h5>How many AXB tokens have been issued? Can any additional tokens be created?</h5>



											<p>There is a total of 3,000,000,000 AXB tokens, 63% of which (1,890,000,000) will be available for sale. No additional tokens will be created.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">



											<h5>What will happen to any unsold tokens?</h5>



											<p>All unsold tokens in the TGE will be burned.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">



											<h5>When will the tokens begin to trade on exchanges?</h5>



											<p>Once the TGE has finished and the tokens have been distributed, AXB will be listed on several prominent exchanges. Please follow our social media channels for the official announcements.</p>



										</div>



									</div><!-- .col  -->



									<!--<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">



											<h5>Vokalia and Consonantia, there live?</h5>



											<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipis cing elit, quis nostrud exerc itation ullamco.</p>



										</div>-->



									</div><!-- .col  -->



								</div><!-- .row  -->







							<div class="tab-pane fade" id="tab-4">



								<div class="row">



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">



											<h5>Who formed us in his own image?</h5>



											<p>Ut enim ad minim veniam, quis nostrud exerc itation ullamco. Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">



											<h5>While the lovely valley teems with vapour?</h5>



											<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipis cing elit, quis nostrud exerc itation ullamco.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">



											<h5>Which was created for the bliss of souls like mine?</h5>



											<p>ICO Crypto - is unique blockchain platform; that is secure, smart and easy-to-use platform, and completely disrupting the way businesses raise capital,  and the way investors buy and sell. </p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">



											<h5>Wonderful serenity has taken of my entire soul?</h5>



											<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc itation ullamco.</p>



										</div>



									</div><!-- .col  -->



								</div><!-- .row  -->



							</div><!-- End tab-pane -->



							<div class="tab-pane fade" id="tab-5">



								<div class="row">



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">



											<h5>Illustrated magazine and housed in a nice?</h5>



											<p>ICO Crypto - is unique blockchain platform; that is secure, smart and easy-to-use platform, and completely disrupting the way businesses raise capital,  and the way investors buy and sell. </p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">



											<h5>Samsa was a travelling salesman and above it there?</h5>



											<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc itation ullamco.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">



											<h5>Slightly domed and divided by arches?</h5>



											<p>Ut enim ad minim veniam, quis nostrud exerc itation ullamco. Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>



										</div>



									</div><!-- .col  -->



									<div class="col-md-6">



										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">



											<h5>One morning, when Gregor Samsa?</h5>



											<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipis cing elit, quis nostrud exerc itation ullamco.</p>



										</div>



									</div><!-- .col  -->



								</div><!-- .row  -->



							</div><!-- End tab-pane -->



						</div><!-- End tab-content -->



					</div><!-- End tab-custom -->



				</div><!-- .col -->



			</div><!-- .row -->



		</div><!-- End container -->



		<div class="mask-ov-left mask-ov-s5"></div><!-- .mask overlay -->



	</div>



	<!-- End Section -->







	<!-- Start Section -->



	<div class="section subscribe-section-s2 section-bg section-dark" id="join" style="background: linear-gradient(180deg, #FFF 50%, #15257b 50%);">

		<div class="container">

			<div class="subscribe-box animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">

                <div class="row">

                    <div class="col-lg-6">

                        <div class="heading-sm-s3">

                            <h5>Join our newsletter for updates</h5>

                            <p>Don't miss out on the token sale! </p>

                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="gaps size-2x"></div>

                        <form id="mc-embedded-subscribe-form" action="https://abeefoundation.us19.list-manage.com/subscribe/post?u=97723e04cbee8406e2a51a261&amp;id=745ee19d9e"  method="post" class="subscription-form inline-form-s2" name="mc-embedded-subscribe-form"  target="_blank" novalidate>

                            <input type="text" name="EMAIL" class="input-round-s2 required email" placeholder="Enter your email address" >

                            <input type="text" class="d-none" name="form-anti-honeypot" id="mce-EMAIL" value="">

                            <button type="submit" class="btn" name="subscribe" value="Subscribe">Keep me updated</button>

                            <div class="subscribe-results"></div>

                        </form>

                    </div>

                </div>

			</div>

		</div>

	</div>



	<!-- End Section -->









	<!-- Start Section -->



	<div class="section section-pad section-bg-dark" id="partners">



		<div class="container">



			<div class="row text-center">



				<div class="col-md-6 offset-md-3">



					<div class="section-head">



						<h6 class="section-title-sm animated" data-animate="fadeInUp" data-delay=".0">As seen in</h6>



					</div>



				</div>



			</div><!-- .row  -->



			<div class="partner-list">

				<div class="single-partner animated" data-animate="fadeInUp" data-delay=".1">

					<img src="assets/images/PBJ_logo.png" alt="partner">

				</div>

				<div class="single-partner animated" data-animate="fadeInUp" data-delay=".2">

					<img src="assets/images/CN-LOGO.png" alt="partner">

				</div>

				<div class="single-partner animated" data-animate="fadeInUp" data-delay=".3">


          <img src="assets/images/New-Dash-Force-News-Logo.png" alt="partner">


        </div>



					<!--</div><!-- .col  -->



					<!--<div class="col-sm col-6">



						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".4">



							<img src="images/partner-xs-d.png" alt="partner">



						</div>



					</div><!-- .col  -->



					<!--<div class="col-sm col-6">



						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".5">



							<img src="images/partner-xs-e.png" alt="partner">



						</div>



					</div><!-- .col  -->



					<!--<div class="col-sm col-6">



						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".6">



							<img src="images/partner-xs-f.png" alt="partner">



						</div>



					</div><!-- .col  -->





				<!--<div class="row text-center">



					<div class="col-md-10 offset-md-1">



						<div class="row">



							<div class="col-sm col-6">



								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".7">



									<img src="images/partner-sm-a.png" alt="partner">



								</div>



							</div><!-- .col  -->



							<!--<div class="col-sm col-6">



								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".8">



									<img src="images/partner-sm-b.png" alt="partner">



								</div>



							</div><!-- .col  -->



							<!--<div class="col-sm col-6">



								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".9">



									<img src="images/partner-sm-c.png" alt="partner">



								</div>



							</div><!-- .col  -->



							<!--<div class="col-sm col-6">



								<div class="single-partner animated" data-animate="fadeInUp" data-delay="1">



									<img src="images/partner-sm-d.png" alt="partner">



								</div>-->



			</div><!-- .partner-list  -->



		</div><!-- .container  -->



	</div>



	<!-- End Section -->











	<!-- Start Section -->



	<div class="section footer-scetion footer-particle section-pad-sm section-bg-dark">



		<div class="container">



			<div class="footer-flex">


				<div class="flex-item">



					<div class="link-widget animated" data-animate="fadeInUp" data-delay=".8">

						<p>What is ABEE</p>

						 <p class='footer__text'>
						     The ABEE Foundation is funding ABEE Rideshare because of its quintessential balance of centralized and decentralized functions. ABEE Rideshare has created a suite of dynamic features that users want and competitors have failed to deliver on.
						</p>
						<!-- <li><a href="#apps" class="menu-link">App</a></li>



						<li><a href="https://token.abeefoundation.org" class="menu-link">Join TGE</a></li>



						<li><a href="#tokenSale" class="menu-link">TGE</a></li>



						<li><a href="assets/documents/AF whitepaper iv.pdf" class="menu-link">Whitepaper</a></li>



						<li><a href="#roadmap" class="menu-link">Roadmap</a></li>



						<li><a href="#team" class="menu-link">Teams</a></li>



						<li><a href="#faq" class="menu-link">FAQ</a></li>
 -->
					</div>
				</div><!-- .col  -->

				<div class="flex-item">
					<ul class="link-widget animated" data-animate="fadeInUp" data-delay=".8">
						<li><a href="#tokenSale" class="menu-link">TGE</a></li>
						<li><a href="#apps" class="menu-link">APP</a></li>
						<li><a href="#tokenSale" class="menu-link">FAQ</a></li>
					</ul>
				</div><!-- .col  -->
        <div class="flex-item">
					<ul class="social">
						<!--<li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="#"><em class="fab fa-facebook-f"></em></a></li>-->
						<li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="#"><em class="fab fa-twitter"></em></a></li>
						<!--<li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="#"><em class="fab fa-youtube"></em></a></li>-->
						<li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/abeefoundation"><em class="fab fa-github"></em></a></li>
						<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="#"><em class="fab fa-bitcoin"></em></a></li>
						<li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@abeefoundation"><em class="fab fa-medium-m"></em></a></li>
					</ul>
				</div><!-- .col  -->

			</div><!-- .row  -->



			<!-- <div class="gaps size-2x"></div>
 -->


			<div class="row">



				<div class="col-md-7">



					<span class="copyright-text animated" data-animate="fadeInUp" data-delay=".9">



						Copyright &copy; 2018, ABEE Foundation.



					</span>



				</div><!-- .col  -->



				<!-- class="col-md-5 text-right mobile-left">



					<ul class="footer-links animated" data-animate="fadeInUp" data-delay="1">



						<li><a href="#">Privacy Policy</a></li>



						<li><a href="#">Terms &amp; Conditions</a></li>



						<li>



							<div class="language-switcher">



								<a href="#" data-toggle="dropdown">EN</a>



								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">



									<a class="dropdown-item" href="#">FR</a>



									<a class="dropdown-item" href="#">CH</a>



									<a class="dropdown-item" href="#">BR</a>



								</div>



							</div>



						</li>



					</ul>



				</div> -->



			</div><!-- .row  -->



		</div><!-- .container  -->



	</div>



	<!-- End Section -->







	<!-- Preloader !remove please if you do not want -->



	<div id="preloader">



		<div id="loader"></div>



		<div class="loader-section loader-top"></div>



   		<div class="loader-section loader-bottom"></div>



	</div>



	<!-- Preloader End -->







	<!-- JavaScript (include all script here) -->



	<script src="assets/js/jquery.bundle.js?ver=123"></script>



	<script src="assets/js/script.js?ver=123"></script>



	<script src="assets/js/telega.js?v=<?=time();?>"></script>



<!-- Yandex.Metrika counter -->

<script type="text/javascript" >

    (function (d, w, c) {

        (w[c] = w[c] || []).push(function() {

            try {

                w.yaCounter50036398 = new Ya.Metrika2({

                    id:50036398,

                    clickmap:true,

                    trackLinks:true,

                    accurateTrackBounce:true,

                    webvisor:true

                });

            } catch(e) { }

        });



        var n = d.getElementsByTagName("script")[0],

            s = d.createElement("script"),

            f = function () { n.parentNode.insertBefore(s, n); };

        s.type = "text/javascript";

        s.async = true;

        s.src = "https://mc.yandex.ru/metrika/tag.js";



        if (w.opera == "[object Opera]") {

            d.addEventListener("DOMContentLoaded", f, false);

        } else { f(); }

    })(document, window, "yandex_metrika_callbacks2");

</script>

<noscript><div><img src="https://mc.yandex.ru/watch/50036398" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<!-- /Yandex.Metrika counter -->



</body>



</html>