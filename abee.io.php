<?php
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}
?>
<!DOCTYPE html>

<html lang="en" class="js">

<head>

	<meta charset="utf-8">

	<meta name="author" content="Softnio">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="ABEE Rideshare">

	<!-- Fav Icon  -->

	<link rel="shortcut icon" href="images/abee6.png">

	<!-- Site Title  -->

	<title>ABEE Rideshare - Here For You</title>

	<!-- Vendor Bundle CSS -->

	<link rel="stylesheet" href="assets/css/vendor.bundle2.css?v=<?=time();?>" >

	<!-- Custom styles for this template -->

	<link rel="stylesheet" href="assets/css/style.css?v=<?=time();?>" >

	<link rel="stylesheet" href="assets/css/theme.css?v=<?=time();?>" >
	
	

	

</head>



<body class="theme-lavendar io-lavendar" data-spy="scroll" data-target="#mainnav" data-offset="80">



	<!-- Header --> 

	<header class="site-header is-sticky">

		<!-- Navbar -->

		<div class="navbar navbar-expand-lg is-transparent" id="mainnav">

			<nav class="container">



				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./" id="logo" >

					<img class="logo logo-dark" alt="logo" src="images/ABEERide_logo2.png" srcset="images/ABEERide_logo2.png 2x">

					<img class="logo logo-light" alt="logo" src="images/ABEERide_logo.png" srcset="images/ABEERide_logo.png 2x">

				</a>

				

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">

					<span class="navbar-toggler-icon">

						<span class="ti ti-align-justify"></span>

					</span>

				</button>



				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">

					<ul class="navbar-nav animated remove-animation fadeInDown" data-animate="fadeInDown" data-delay=".75" style="visibility: visible; animation-delay: 0.75s; margin-right: 43%;">

						<li class="nav-item"><a class="nav-link menu-link" href="#about">Riders</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#why">Drivers</a></li>

						

					</ul>

					<ul class="navbar-btns animated remove-animation fadeInDown" data-animate="fadeInDown" data-delay=".85" style="visibility: visible; animation-delay: 0.85s;">

						<li class="nav-item"><a class="nav-link btn btn-white btn-sm menu-link" href="#">Become A Driver</a></li>
						
						<li class="nav-item"><a class="nav-link btn btn-white btn-sm menu-link" href="#"><em class="ti ti-download"></em> Download The App</a></li>

						<!--<li class="nav-item">

							<a class="btn btn-sm btn-outline" href="#" ><em class="ti ti-download"></em> Download The App</a>

							

						</li>-->

					</ul>

				</div>

			</nav>

		</div>

		<!-- End Navbar -->



		<!-- Banner/Slider -->

		<div id="header" class="banner">

			<div class="banner-rounded-bg">

				<!--<span class="banner-shade-1">

					<span class="banner-shade-2">

						<span class="banner-shade-3"></span>

					</span>

				</span>
-->
			</div>

			<div class="container">

				<div class="banner-content">

					<div class="row text-center">

						<div class="col-lg-12">

							<div class="header-txt">

								<h1 class="animated" data-animate="fadeInUp" data-delay="1.25">Start Earning In The<br class="d-none d-xl-block"> Good-Faith Economy</h1>
								<!--<p class="lead animated" data-animate="fadeInUp" data-delay="1.35">ICO Crypto is the best selling, modern, clean and elegant design, created for ICO<br class="d-none d-xl-block"> development agencies. It’s Include all necessary features that fit for ICOs.</p>-->

							</div>

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .banner-content  -->

				<div class="row justify-content-center text-center" >
				    
					<div class="col-lg-8">
							<!--<img src="images/verticalplaceholderimage.png" style="padding-top: 55px;" />-->
																										 
							<!--<img src="images/gfx-white_2x.png" style="padding-top: 135px;" />--> 
																									 
					</div>
					<div class="col-lg-4">

						

						<div class="gaps"></div>

						<h4 class="animated" data-animate="fadeInUp" data-delay="1.55">Sign Up To Drive</h4>

						<div class="gaps size-0-5x"></div>

						
						
						<div class="token-box shadow animated fadeInUp" data-animate="fadeInUp" data-delay="1.65" style="overflow: hidden; visibility: visible; animation-delay: 1.65s;">

							

                           

                          

                               <div >

                                   <nav>

                                      <div class="nav nav-tabs justify-content-center" id="nav-tab">

                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#cont-signup">Driver</a>

                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#cont-signin">Rider</a>

                                      </div>

                                    </nav>

                                    <div class="tab-content" id="nav-tabContent">

                                        <div class="tab-pane fade show active" id="cont-signup" role="tabpanel">

                                         

                                            <form action="#" class="signup-form" abineguid="E6269D5F2132431CAE02DD7A3927C879">

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Name" class="input-border-simple">

                                                    <em class="fas fa-user"></em>

                                                </div>

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Email" class="input-border-simple">

                                                    <em class="fas fa-envelope"></em>

                                                </div>

                                                

                                                    

                                                        <div class="input-item">

                                                            <input type="text" placeholder="Telephone" class="input-border-simple">

                                                            <em class="fas fa-phone"></em>

                                                        </div>

												<div class="input-item">

                                                            <input type="text" placeholder="City" class="input-border-simple">

                                                            <em class="fas fa-building"></em>

                                                        </div>
                                                   

                                                    

                                                        <div class="input-item">

                                                            <input type="text" placeholder="How did you hear about us" class="input-border-simple">

                                                            <em class="fas fa-info"></em>

                                                        </div>

                                                   
                                               

                                             <div class="gaps d-xl-none"></div>

                                                <div class="d-xl-flex justify-content-between align-items-center">

                                                    <button class="btn btn-sm input-block-level form-control">Sign Up</button>

                                                  

                                                    

                                                </div>

                                            </form>

                                        </div>

                                        <div class="tab-pane fade" id="cont-signin" role="tabpanel">

                                            <form action="#" class="signup-form" abineguid="0859255A5F81407083FC638D93678459">

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Name" class="input-border-simple">

                                                    <em class="fas fa-user"></em>

                                                </div>

                                                <div class="input-item">

                                                    <input type="text" placeholder="Your Email" class="input-border-simple">

                                                    <em class="fas fa-envelope"></em>

                                                </div>

                                                

                                                    

                                                        <div class="input-item">

                                                            <input type="text" placeholder="Telephone" class="input-border-simple">

                                                            <em class="fas fa-phone"></em>

                                                        </div>

												<div class="input-item">

                                                            <input type="text" placeholder="City" class="input-border-simple">

                                                            <em class="fas fa-building"></em>

                                                        </div>
                                                   

                                                    

                                                        <div class="input-item">

                                                            <input type="text" placeholder="How did you hear about us" class="input-border-simple">

                                                            <em class="fas fa-info"></em>

                                                        </div>

                                                   
                                               

                                             <div class="gaps d-xl-none"></div>

                                                <div class="d-xl-flex justify-content-between align-items-center">

                                                    <button class="btn btn-sm input-block-level form-control">Sign Up</button>

                                                  

                                                    

                                                </div>

                                            </form>

                                            
                                        </div>

                                    </div>

                               </div>

                           

                       

						</div>
						<!-- EO token box -->

					</div>

				</div>

			</div><!-- .container  -->

		</div>

	</header>

	<!-- End Header -->






	

	



	

	



	

	

	<!-- Start Section -->

	<div class="section reason-scetion no-pb section-pad section-bg-light" id="why">

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Why ABEE?</h2>

			</div>

			<div class="shadow round-md plr-x4">

				<div class="gaps size-2x"></div>

				<div class="row text-center">

					<div class="col-md-6 res-m-bttm">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".2">

							<img src="images/abeeio/Icon_3.png" alt="">

							<h5>Good-Faith Business</h5>

							<p>ABEE takes 60% less than the competition. By using ABEE, you will directly participate in the sharing economy of the future.</p>

						</div>

					</div>

					<div class="col-md-6 res-m-bttm">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".3">

							<img src="images/abeeio/02.png" alt="">

							<h5>User Voting</h5>

							<p>The ABEE Forum allows all users of the platform to vote on upgrades they would like to see in the next app update.</p>

						</div>

					</div>

					<div class="col-md-6 res-m-bttm">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".4">

							<img src="images/abeeio/03.png" alt="">

							<h5>Peer-to-Peer Payments</h5>

							<p>With Dash, users can pay their drivers immediately with one of the world's best cryptocurrencies.</p>

						</div>

					</div>

					<div class="col-md-6">

						<div class="reason-item animated" data-animate="fadeInUp" data-delay=".5">

							<img src="images/abeeio/f2.png" alt="">

							<h5>On Trip Ads</h5>

							<p>Every rider can receive discounts on their rides by providing their feedback on our ad platform.</p>

						</div>

					</div>

				</div><!-- .row  -->

				<div class="gaps size-2x"></div>

			</div>

		</div><!-- .container  -->

	</div>

	<!-- End Section --> 

	

	

	<!-- Start Section -->

	<div class="section process-scetion section-pad section-bg-light">

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">How ABEE Works</h2>

			</div>

			<div class="row align-items-center">

				<div class="col-lg-7">

					<div class="slider-nav">

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".5"><em class="ikon ikon-shiled"></em></div>
						
						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".2"><em class="ikon ikon-paricle"></em></div>
						
						
						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".4"><em class="ikon ikon-target"></em></div>


						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".3"><em class="ikon ikon-connect"></em></div>

						<div class="slider-nav-item owl-dots animated" data-animate="fadeInUp" data-delay=".6"><em class="ikon ikon-id-card"></em></div>

					</div>

				</div><!-- .col  -->

				<div class="col-lg-5">

					<div class="slider-pane animated" data-animate="fadeInUp" data-delay=".7">

						<div class="slider-pane-item pane-item-1">

							<h5 class="animate-up delay-5ms">Choose your destination</h5>

							<p class="animate-up delay-6ms"></p>

						</div>

						<div class="slider-pane-item pane-item-2">

							<h5 class="animate-up delay-5ms">See your cost and ETA upfront</h5>

							<p class="animate-up delay-6ms"></p>

						</div>

						<div class="slider-pane-item pane-item-3">

							<h5 class="animate-up delay-5ms">Get a ride in minutes</h5>

							<p class="animate-up delay-6ms"></p>

						</div>

						<div class="slider-pane-item pane-item-4">

							<h5 class="animate-up delay-5ms">Participate in On Trip Ads</h5>

							<p class="animate-up delay-6ms"></p>

						</div>

						<div class="slider-pane-item pane-item-5">

							<h5 class="animate-up delay-5ms">Pay in app when you arrive</h5>

							<p class="animate-up delay-6ms"></p>

						</div>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section --> 

	

	


	

	

	<!-- Start Section -->

	<div class="section features-section section-pad no-pb section-bg-lavendar">

		<div class="shade-wraper">

			<div class="container">

				<div class="shade-left"></div>

				<div class="shade-right"></div>

			</div>

		</div>

		<div class="container">

			<div class="section-head-s3 text-center">

				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">ABEE App</h2>

			</div>

			<div class="row">

				<div class="col-lg-4 col-md-6 col-sm-12 order-lg-0 order-md-0">

					<div class="gaps"></div>

					<div class="features-item left animated" data-animate="fadeInUp" data-delay=".2">

						<em class="ti ti-wallet"></em>
						<!--<em class="ti ti-wallet"></em>-->
						
						<!--<img src="images/signal.png"  alt="" style="border-radius: 50%; display: inline-block;" />-->

						<h5>Multiple Payment Options</h5>

						<p>Riders can join the crypto evolution and pay with Dash, an instant cryptocurrency or pay with debit or credit cards.</p>

					</div>

					<div class="features-item left animated" data-animate="fadeInUp" data-delay=".3">

						<!--<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>-->
						
						<em class="ti ti-gift"></em>

						<h5>On Trip Ad Rewards</h5>

						<p>By providing valuable feedback in the ABEE ad portal while on a trip, you will receive credits for discounts on future rides.</p>

					</div>

				</div>

				<div class="col-lg-4 col-md-12 col-sm-12 order-lg-1 order-md-2 order-last mt-auto text-center">

					<div class="features-photo animated" data-animate="fadeInUp" data-delay=".6">

						<!--<img src="images/mobile-app-a.png" alt="mobile app">-->
						<img src="images/abeeio/Mobile_Screen_v2.png" alt="mobile app">

					</div>

				</div>

				<div class="col-lg-4 col-md-6 col-sm-12 order-lg-2 order-md-1">

					<div class="gaps"></div>

					<div class="features-item animated" data-animate="fadeInUp" data-delay=".4">

						<!--<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>-->
						
						
						<em class="ti"><img src="images/hailing2.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>

						<h5>Catch A Ride</h5>

						<p>You can walk up to any inactive ABEE vehicle, scan their QR code, and hop in to your destination.</p>

					</div>

					<div class="features-item animated" data-animate="fadeInUp" data-delay=".5">

						<!--<em class="ti"><img src="images/signal_main.png"  alt="" style="border-radius: 50%; display: inline-block;" /></em>-->
						
						<em class="ti ti-id-badge"></em>

						<h5>Favorite Drivers</h5>

						<p>To build community within ABEE, riders can select or schedule a ride with a previous driver they had a memorable experience with.</p>

					</div>

				</div>

			</div>

		</div><!-- .container  -->

	</div>

	<!-- End Section -->


 

	<!-- Start Section -->
	<div class="section media-scetion no-pb section-pad section-bg-light" id="media">
		<div class="container">
			<div class="section-head-s3 text-center">
				<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Press</h2>
			</div>
			<div class="has-carousel animated" data-animate="fadeInUp" data-delay=".2" data-items="3" data-dots="true" data-navs="false">
				<div class="media-box">
					<img src="images/abeeio/phoenix-business-journal-logo.jpg" alt="media">
					<h5 class="media-heading"><a href="https://www.bizjournals.com/phoenix/news/2018/07/09/local-rideshare-startup-will-offer-scottsdale.html">Local rideshare startup will offer Scottsdale-based cryptocurrency payment options</a></h5><!--
					<p>Energy dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>-->
				</div>
				<div class="media-box">
					<img src="images/abeeio/dashforcenews_logo.png" alt="media">
					<h5 class="media-heading"><a href="https://www.dashforcenews.com/ridesharing-platform-abee-to-integrate-dash/">Ridesharing Platform, ABEE, to Integrate Dash</a></h5><!--
					<p>Digital coin untsed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim </p>-->
				</div>
				<div class="media-box">
					<img src="images/abeeio/CN-LOGO.gif" alt="media">
					<h5 class="media-heading"><a href="https://cronkitenews.azpbs.org/2018/04/02/tech-behind-cryptocurrency-craze-may-disrupt-medicine-housing-and-id-security/">Tech behind cryptocurrency craze could disrupt medicine, housing and ID security</a></h5><!--
					<p>Business idea sunt in minim veniam, quis nostrud exercitation ullamco laboris ea commodo conseq.</p>-->
				</div>
				<!--<div class="media-box">
					<img src="images/partner-md-d.png" alt="media">
					<h5 class="media-heading"><a href="#">ICOX coin build with modern blockchain technology</a></h5>
					<p>Energy dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
				</div>
				<div class="media-box">
					<img src="images/partner-md-e.png" alt="media">
					<h5 class="media-heading"><a href="#">ICOX coin is more powerful cryptocurrencies</a></h5>
					<p>Digital coin untsed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim </p>
				</div>-->
			</div>
		</div>
	</div>
	<!-- End Section -->
	
	
	<!-- Start Section -->
	<div class="section subscribe-scetion section-pad section-bg-light" id="subscribe" style="padding-top: 25px;">
		<div class="container">
			<div class="row  justify-content-center text-center">
				<div class="col-lg-6 res-l-bttm">
					<div class="subscribe-rounded">
						<h5 class="animated" data-animate="fadeInUp" data-delay=".1">Don’t miss out on the launch of ABEE! </h5>
						<p class="animated" data-animate="fadeInUp" data-delay=".2">Be one of the first thousand to sign up and win a free ride!</p>
						<form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form animated" data-animate="fadeInUp" data-delay=".3">
							<input type="text" name="youremail" class="input-round required email" placeholder="Enter your email" >
							<input type="text" class="d-none" name="form-anti-honeypot" value="">
							<button type="submit" class="btn btn-plane">Subscribe</button>
							<div class="subscribe-results"></div>
						</form>
						<!--<div class="gaps"></div>
						<a href="#" class="btn btn-simple animated" data-animate="fadeInUp" data-delay=".4"> <em class="fa fa-paper-plane"></em> Join us on Telegram</a>-->
					</div>
				</div><!-- .col  -->
			</div><!-- .row  -->
		</div><!-- .container  -->
	</div>
	<!-- End Section -->
	
	<!-- Start Section -->
	<div class="section footer-scetion footer-lavendar">
		<div class="social-overlap">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-10">
						<ul class="social-bar animated" data-animate="fadeInUp" data-delay=".1">
							<li>Follow us</li>
							<li><a href="https://www.facebook.com/abee.rideshare.3"><em class="fab fa-facebook-f"></em></a></li>
							<li><a href="https://twitter.com/officialABEE"><em class="fab fa-twitter"></em></a></li>
							<li><a href="https://www.instagram.com/abeerideshare/"><em class="fab fa-instagram"></em></a></li>
							<!--<li><a href="#"><em class="fab fa-github"></em></a></li>
							<li><a href="#"><em class="fab fa-bitcoin"></em></a></li>
							<li><a href="#"><em class="fab fa-medium-m"></em></a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="gaps size-4x"></div>
		<div class="gaps size-2x d-none d-sm-block"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-lg-3 res-m-bttm">
					<a class="footer-brand animated" data-animate="fadeInUp" data-delay=".2" href="./">
						<img class="logo logo-light" alt="logo" src="images/abeeio/abee_white.png" srcset="images/abeeio/abee_white.png 2x">
					</a>
				</div><!-- .col  -->
				<div class="col-sm-4 col-lg-2 res-l-bttm">
					<ul class="link-widget one-column animated" data-animate="fadeInUp" data-delay=".3">
						<li><a href="#token" class="menu-link"><strong>Driver</strong></a></li>
						<li><a href="#roadmap" class="menu-link">Sign Up To Drive</a></li>
						<li><a href="#about" class="menu-link">Requirements</a></li><!--
						<li><a href="#why" class="menu-link">why / How ?</a></li>
						<li><a href="#media" class="menu-link">Media</a></li>-->
					</ul>
				</div><!-- .col  -->
				<div class="col-sm-4 col-lg-2 res-l-bttm">
					<ul class="link-widget one-column animated" data-animate="fadeInUp" data-delay=".4">
						<li><a href="#team" class="menu-link"><strong>Rider</strong></a></li>
						<li><a href="#about" class="menu-link">Cities</a></li>
						<li><a href="#contact" class="menu-link">Safety</a></li><!--
						<li><a href="#faq" class="menu-link">Faqs</a></li>
						<li><a href="#subscribe" class="menu-link">Join Us</a></li>-->
					</ul>
				</div><!-- .col  -->
				<div class="col-sm-4 col-lg-3">
					<ul class="address-widget animated" data-animate="fadeInUp" data-delay=".5">
						<li>1 E Washington Street <br> Phoenix, Arizona 85004</li>
						<!--<li>+505-735-0177</li>-->
						<li>contact@abee.io</li>
					</ul>
				</div><!-- .col  -->
			</div><!-- .row  -->
		</div><!-- .container  -->
		
		<hr class="hr-line"><!-- .hr-line  -->
		
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<span class="copyright-text">
						Copyright &copy; 2018, ABEE Rideshare.
					</span>
				</div><!-- .col  -->
				<div class="col-md-5 text-right mobile-left">
					<ul class="footer-links">
						<li><a href="#">Privacy</a></li>
						<li><a href="#">Terms</a></li>
					</ul>
				</div><!-- .col  -->
			</div><!-- .row  -->
		</div><!-- .container  -->
		<div class="gaps size-2x"></div>
	</div>
	<!-- End Section -->

	<!-- Preloader !remove please if you do not want -->
	<div id="preloader">
		<div id="loader"></div>
		<div class="loader-section loader-top"></div>
   		<div class="loader-section loader-bottom"></div>
	</div>
	<!-- Preloader End -->

	<!-- JavaScript (include all script here) -->
	<script src="assets/js/jquery.bundle.js?ver=123"></script>
	<script src="assets/js/script.js?ver=123"></script>

</body>
</html>