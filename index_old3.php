<!DOCTYPE html>

<html lang="zxx" class="js">

<head>

	

    

	<meta charset="utf-8">

	<meta name="author" content="ABEE Foundation">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="ABEE Foundation, funding rideshare 3.0">

	<!-- Fav Icon  -->

	<link rel="shortcut icon" href="images/TAF.png">

	<!-- Site Title  -->

	<title>The ABEE Foundation, Funding Rideshare 3.0</title>

	<!-- Vendor Bundle CSS -->

	<link rel="stylesheet" href="assets/css/vendor.bundle.css?v=<?=time();?>">

	<!-- Custom styles for this template -->

	<link rel="stylesheet" href="assets/css/style.css?v=<?=time();?>">

	<link rel="stylesheet" href="assets/css/theme-java.css?v=<?=time();?>">

	

</head>



<body class="theme-dark io-azure io-azure-pro" data-spy="scroll" data-target="#mainnav" data-offset="80">



	<!-- Header --> 

	<header class="site-header is-sticky">

		<!-- Navbar -->

		<div class="navbar navbar-expand-lg is-transparent" id="mainnav">

			<nav class="container">

				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="./">

					<img class="logo logo-dark" alt="logo" src="images/ABEElogo.png" srcset="images/ABEElogo.png 2x">

					<img class="logo logo-light" alt="logo" src="images/ABEElogo.png" srcset="images/ABEElogo.png 2x">

				</a>

				

				<!--<div class="language-switcher animated" data-animate="fadeInDown" data-delay=".75">

					<a href="#" data-toggle="dropdown">EN</a>

					<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

						<a class="dropdown-item" href="#">FR</a>

						<a class="dropdown-item" href="#">CH</a>

						<a class="dropdown-item" href="#">BR</a>

					</div>

				</div>-->

				

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle">

					<span class="navbar-toggler-icon">

						<span class="ti ti-align-justify"></span>

					</span>

				</button>



				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">

					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".9">

						<li class="nav-item"><a class="nav-link menu-link" href="#intro">ABEE<span class="sr-only">(current)</span></a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#tokenSale">TGE</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#roadmap">Roadmap</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#apps">Apps</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#team">Team</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#faq">Faq</a></li>

						<li class="nav-item"><a class="nav-link menu-link" href="#contact">Contact</a></li>

					</ul>

					<ul class="navbar-nav navbar-btns animated remove-animation" data-animate="fadeInDown" data-delay="1.15">

						<li class="nav-item"><a class="nav-link btn btn-sm btn-outline menu-link" target= "_blank" href="https://token.abeefoundation.org">ABEE Tokens</a></li>

					

					</ul>

				</div>

			</nav>

		</div>

		<!-- End Navbar -->



		<!-- Banner/Slider -->

		<div id="header" class="banner banner-full d-flex align-items-center">

			<div class="container">

				<div class="banner-content">

					<div class="row align-items-center mobile-center">

						<div class="col-lg-6 col-md-12 order-lg-first">

							<div class="header-txt">

								<h1 class="animated" data-animate="fadeInUp" data-delay="1.55"><br class="d-none d-xl-block"> <br class="d-none d-xl-block">The ABEE Foundation</h1>

								<p class="lead animated" data-animate="fadeInUp" data-delay="1.65">Funding Rideshare 3.0</p>

								<ul class="btns animated" data-animate="fadeInUp" data-delay="1.75">

									<li><a href="assets/documents/ABEE_Foundation_Main_WP.pdf" class="btn btn-alt">Whitepaper</a></li>

									<li><a target= "_blank" href="https://token.abeefoundation.org" class="btn btn-alt">ABEE Tokens</a></li>

								</ul>

							</div>

						</div><!-- .col  -->

						<div class="col-lg-6 col-md-12 order-first res-m-bttm-lg">

                            <div class="video-graph res-m-btm animated" data-animate="fadeInUp" data-delay=".1">

						<img src="images/gfx-white_2x.png" alt="graph">

						<a href="https://www.youtube.com/watch?v=SSo_EIwHSd4" class="play-btn-alt video-play">

							<em class="fa fa-play"></em>

							<!--<span>How it work</span>-->

						</a>

					</div>

                            

							<!--<div class="header-image-alt animated" data-animate="fadeInRight" data-delay="1.25">

								<img src="images/header-image-blue.png" alt="header">

							</div>-->

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .banner-content  -->

			</div><!-- .container  -->

		</div>

		<!-- End Banner/Slider -->

		<div class="header-partners">

			<div class="container">

				<div class="row align-items-center">

					<ul class="partner-list-s2 d-flex flex-wrap align-items-center">

						<li class="animated" data-animate="fadeInUp" data-delay="1.85">ABEE Foundation & ABEE Rideshare's <br> Partners</li>

						<li class="animated" data-animate="fadeInUp" data-delay="1.9"><a href="#"><img src="assets/images/dash_new.png" width="50%" alt="partner"></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay="1.95"><a href="#"><img src="assets/images/chain_new.png" alt="partner"></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay="2"><a href="#"><img src="images/partner-xs-light-c.png" alt="partner"></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay="2.05"><a href="#"><img src="images/partner-xs-light-d.png" alt="partner"></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay="2.1"><a href="#"><img src="images/partner-xs-light-e.png" alt="partner"></a></li>

					</ul>

				</div><!-- .row  -->

			</div><!-- .container  -->

		</div><!-- .header-partners  -->

		<a href="#intro" class="scroll-down menu-link">SCROLL DOWN</a>

	</header>

	<!-- End Header -->



	<!-- Start Section -->

	<div class="section section-pad section-bg section-pro nopb into-section" id="intro">

		<div class="container">

			<div class="row align-items-center">

				<div class="col-md-5 offset-md-1">

					<div class="video-graph res-m-btm animated" data-animate="fadeInUp" data-delay=".1">

						<img src="images/ABEEcircle.png" alt="graph">

						<!--<a href="https://www.youtube.com/watch?v=SSo_EIwHSd4" class="play-btn-alt video-play">

							<em class="fa fa-play"></em>

							<span>How it work</span>

						</a>-->

					</div>

				</div><!-- .col  -->

				<div class="col-md-6 order-md-first order-last">

					<div class="text-block">

						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Why ABEE Rideshare?</h6>

						<h2 class="animated" data-animate="fadeInUp" data-delay=".1">Setting the stage for Rideshare 3.0</h2>

						<p class="lead animated" data-animate="fadeInUp" data-delay=".2">The disruption of a $50 billion industry set to surge 800% by 2030.</p>

						<p class="animated" data-animate="fadeInUp" data-delay=".3">The ABEE Foundation is funding ABEE Rideshare because of its quintessential balance of centralized and decentralized functions. After months of research and interviewing we have found the perfect team with the right solutions to usher in the next era of rideshare. </p>

						<p class="animated" data-animate="fadeInUp" data-delay=".4">ABEE Rideshare has created a suite of dynamic features that users want and competitors have failed to deliver on.  Rideshare companies have paved the way, providing important insight so ABEE can avoid their mistakes and expand on the industry without the accumulation of enormous debt, liabilities and unnecessary overhead.</p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .conatiner  -->

	</div>

	<!-- Start Section -->





	<!-- Start Section -->

	<div class="section section-pad section-bg section-pro">

		<div class="container">

			<div class="row text-center">

				<div class="col-lg-8 offset-lg-2">

					<div class="section-head-s2">

						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0"></h6>

						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Competitive Advantage</h2>

						<p class="animated" data-animate="fadeInUp" data-delay=".2">We are proud to stand behind ABEE, the good faith business practices and stragtetic balance of services make the entity stand out from the rest.</p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

			<div class="row text-center">

				<div class="col-md-4">

					<div class="features-box animated" data-animate="fadeInUp" data-delay=".2">

						<img src="images/Icon_1.png" alt="features">

						<h5>Superior User Experience</h5>

						<p>ABEE Rideshare takes 50% less than major competitors, while giving users added benefits and control.</p>

					</div>

				</div><!-- .col  -->

				<div class="col-md-4">

					<div class="features-box animated" data-animate="fadeInUp" data-delay=".3">

						<img src="images/Icon_3.png" alt="features">

						<h5>Good Faith Business</h5>

						<p>Structuring a business from the ground up that is catering to users’ needs in a mutually beneficial and sustainable manner.</p>

					</div>

				</div><!-- .col  -->

				<div class="col-md-4">

					<div class="features-box animated" data-animate="fadeInUp" data-delay=".4">

						<img src="images/Icon_2.png" alt="features">

						<h5>Crypto to the masses</h5>

						<p>ABEE Rideshare found a subtle and impactful way to integrate decentralized blockchain services and crypto currency payments into a flourishing industry.</p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

			<div class="gaps size-2x d-none d-md-block"></div>

			<div class="row text-center justify-content-center">

				<div class="col-md-12">

					<ul class="btns">

						<li class="animated" data-animate="fadeInUp" data-delay=".0"><a href="assets/documents/ABEE_Foundation_Main_WP.pdf" class="btn btn-alt">Download WHitepaper</a></li>

						<li class="animated" data-animate="fadeInUp" data-delay=".05"><a href="#" class="btn btn-alt">Apply To Get Whitelisted</a></li>

					</ul>

					<div class="gaps size-1x d-none d-md-block"></div>

					<div class="animated" data-animate="fadeInUp" data-delay=".1">

						<a href="https://t.me/joinchat/FqphaE8-dbFUcHR76ELcsA" class="btn btn-simple"> <em class="fa fa-paper-plane"></em> Join us on Telegram</a>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .conatiner  -->

		<div class="mask-ov-right mask-ov-s2"></div><!-- .mask overlay -->

	</div>

	<!-- Start Section -->





	<!-- Start Section -->

	<div class="section section-pad section-bg token-section section-gradiant" id="tokenSale">

		<div class="container">

			<div class="row text-center">

				<div class="col-lg-6 offset-lg-3">

					<div class="section-head-s2">

						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">ABEE Tokens </h6>

						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Pre &amp; Main TGE</h2>

						<p class="animated" data-animate="fadeInUp" data-delay=".2">Participate in the TGE hosted by The ABEE Foundation. The ABEE token is based on the Qtum blockchain. It is safest to store QRC20s in the Qtum Web Wallet with Ledger Nano S or Qtum Electrum wallet with Trezor. </p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

			<div class="row align-items-center">

				<div class="col-lg-7 res-m-bttm">

					<div class="row event-info">

						<div class="col-sm-6">

							<div class="event-single-info animated" data-animate="fadeInUp" data-delay="0">

								<h6>Start</h6>

								<p>Sep 3, 2018 (6:00AM GMT)</p>

							</div>

						</div><!-- .col  -->

						<div class="col-sm-6">

							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".1">

								<h6>ABEE Tokens Up For Contribution</h6>

								<p>1,890,000,000 ABT (63%)</p>

							</div>

						</div><!-- .col  -->

						<div class="col-sm-6">

							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".2">

								<h6>End</h6>

								<p>2 months from soft cap reached or hard cap stop.</p>

							</div>

						</div><!-- .col  -->

						<div class="col-sm-6">

							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".3">

								<h6>Tokens exchange rate</h6>

								<p>1 QTUM = 1040 ABT, 1 BTC = 799680 ABT</p>

							</div>

						</div><!-- .col  -->

						<div class="col-sm-6">

							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".4">

								<h6>Acceptable currencies</h6>

								<p>QTUM, BTC, DASH, ETH</p>

							</div>

						</div><!-- .col  -->

						<div class="col-sm-6">

							<div class="event-single-info animated" data-animate="fadeInUp" data-delay=".5">

								<h6>Minimal transaction amount</h6>

								<p>60 QTUM/ .1 BTC/ 1 ETH/ 3 DASH</p>

							</div>

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .col  -->

				<div class="col-lg-5">

					<div class="countdown-box text-center animated" data-animate="fadeInUp" data-delay=".3">

						<h6>Pre-TGE will start in</h6>

						<div class="token-countdown d-flex align-content-stretch" data-date="2018/09/05"></div>

						<a href="https://token.abeefoundation.org" class="btn btn-alt btn-sm">Contribute now</a>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->



			<div class="gaps size-3x"></div><div class="gaps size-3x d-none d-md-block"></div><!-- .gaps  -->



			<div class="row text-center">

				<div class="col-md-6">

					<div class="single-chart light res-m-btm">

						<h3 class="sub-heading">ABEE Token Allocation</h3>

						<div class="animated" data-animate="fadeInUp" data-delay=".0">

							<img src="images/black chart.png" alt="chart">

						</div>

					</div>

				</div><!-- .col  -->



				<div class="col-md-6">

					<div class="single-chart light">

						<h3 class="sub-heading">TGE Fund Allocation</h3>

						<div class="animated" data-animate="fadeInUp" data-delay=".1">

							<img src="images/white chart.png" alt="chart">

						</div>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->



		</div><!-- .container  -->

	</div>

	<!-- Start Section -->





	<!-- Start Section -->

	<div class="section section-pad section-bg" id="roadmap" style="background-color: #10122d;">
		<div class="container">
		    <div class="row">
		        <div class="col-lg-6">
		            <div class="section-head-s6">
                        <h6 class="heading-sm-s2 animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">TIMELINE</h6>
                        <h2 class="section-title-s6 animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s; color: #1641b5;">Road Map</h2>
                        <p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".3" style="visibility: visible; animation-delay: 0.3s;">Our team working hardly to make archive lorem ipsum dolor sit amet, consectetur amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="gaps size-1x"></div>
                    <div class="gaps size-2x d-none d-md-block"></div>
		        </div>
		    </div>
            <div class="timeline-row timeline-row-odd timeline-row-done animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="visibility: visible; animation-delay: 0.4s;">
               <div class="row no-gutters text-left text-lg-center justify-content-center">
                   <div class="col-lg">
                       <div class="timeline-item timeline-done">
                           <span class="timeline-date">2017 Q1</span>
                           <h6 class="timeline-title">Concept</h6>
                           <ul class="timeline-info">
                               <li>Concept Generation</li>
                               <li>Team Assemble</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item timeline-done">
                           <span class="timeline-date">2017 Q2</span>
                           <h6 class="timeline-title">Research</h6>
                           <ul class="timeline-info">
                               <li>Proving the concept can work</li>
                               <li>Strategic Plan</li>
                               <li>White paper conpletion</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item timeline-done">
                           <span class="timeline-date">2018 Q1</span>
                           <h6 class="timeline-title">Design</h6>
                           <ul class="timeline-info">
                               <li>Platform design and technical demonstration</li>
                               <li>Building the MVP</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item timeline-done">
                           <span class="timeline-date">2018 Q2</span>
                           <h6 class="timeline-title">Pre-Sale</h6>
                           <ul class="timeline-info">
                               <li>Public financing &amp; Seed funding raised</li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
           <div class="timeline-row timeline-row-even animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="visibility: visible; animation-delay: 0.4s;">
               <div class="row no-gutters text-left text-lg-center justify-content-center flex-row-reverse">
                   <div class="col-lg">
                       <div class="timeline-item timeline-current">
                           <span class="timeline-date">2018 Q3</span>
                           <h6 class="timeline-title">Alpha Test</h6>
                           <ul class="timeline-info">
                               <li>In-house testing of functional</li>
                               <li>Prototype published and linked to Ethereum blockchain with real-time scanning</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item">
                           <span class="timeline-date">2018 Q4</span>
                           <h6 class="timeline-title">Token Sale</h6>
                           <ul class="timeline-info">
                               <li>ICO Press Tour</li>
                               <li>Open global sales of ICOblock token</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item">
                           <span class="timeline-date">2019 Q1</span>
                           <h6 class="timeline-title">App Beta Test</h6>
                           <ul class="timeline-info">
                               <li>Private closed beta</li>
                               <li>Open beta launched to public and improvement the app</li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
           <div class="timeline-row timeline-row-odd timeline-row-last mb--x5 animated fadeInUp" data-animate="fadeInUp" data-delay=".4" style="visibility: visible; animation-delay: 0.4s;">
               <div class="row no-gutters text-left text-lg-center justify-content-center">
                   <div class="col-lg">
                       <div class="timeline-item">
                           <span class="timeline-date">2019 Q2</span>
                           <h6 class="timeline-title">Crowdfunding Integration</h6>
                           <ul class="timeline-info">
                               <li>Smart contracts support creators</li>
                               <li>Ethereum tokens support</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item">
                           <span class="timeline-date">2019 Q3</span>
                           <h6 class="timeline-title">Community Benefits</h6>
                           <ul class="timeline-info">
                               <li>Establishing global user base</li>
                               <li>US start retailer selection</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item">
                           <span class="timeline-date">2019 Q4</span>
                           <h6 class="timeline-title">Hardware things</h6>
                           <ul class="timeline-info">
                               <li>Integration of third party controllers</li>
                               <li>Marketplace cooperative module</li>
                           </ul>
                       </div>
                   </div>
                   <div class="col-lg">
                       <div class="timeline-item">
                           <span class="timeline-date">2020 Q1</span>
                           <h6 class="timeline-title">More Operational</h6>
                           <ul class="timeline-info">
                               <li>Integration with Private Chains, More Coin in Wallet</li>
                               <li>New services offered by members or business</li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
           <div class="gaps size-2x d-lg-none"></div>
        </div>
    </div>
	
	

	<!-- Start Section -->





	<!-- Start Section -->

	<div class="section section-pad section-bg-alt section-pro nopb" id="apps">

		<div class="container">

			<div class="row text-center">

				<div class="col-lg-6 offset-lg-3">

					<div class="section-head-s2">

						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0"></h6>

						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">The ABEE Rideshare App</h2>

						<p class="animated" data-animate="fadeInUp" data-delay=".2">The ABEE Rideshare app has the feel of the prominent rideshare companies but with features that the industry has yet to experience.</p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

			

			<div class="row align-items-center">

				<div class="col-md-6 res-m-btm">

					<div class="text-block">

						<p class="animated" data-animate="fadeInUp" data-delay="0">The ABEE Rideshare app provides a suite of features that enables users a more rich and dynamic experience, increasing user controls and accessibilities. </p>

						<ul class="animated" data-animate="fadeInUp" data-delay=".1">

							<li>Earn free or discounted rides every trip with on trip ADs</li>

							<li>Drivers can upgrade to super driver and give passengers discounts</li>

							<li>Dash payments for P2P transactions</li>

							<li>Riders and Drivers can vote in app on desired upgrades</li>

							<li>Scan your drivers vehicle QR code for instant ride requests</li>

						</ul>

						<ul class="btns">

							<li class="animated" data-animate="fadeInUp" data-delay="0"><a href="#" class="btn btn-sm">GET THE APP NOW</a></li>

							<li class="animated" data-animate="fadeInUp" data-delay=".2">

								<a href="#"><em class="fab fa-apple"></em></a>

								<a href="#"><em class="fab fa-android"></em></a>

							</li>

						</ul>

					</div>

				</div><!-- .col  -->

				<div class="col-md-6 mt-auto ">

					<div class="animated" data-animate="fadeInUp" data-delay="0" style="padding-left: 35px; padding-bottom: 35px;">

						<img src="images/app-screen-v2.png" alt="graph">

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

		</div><!-- .container  -->

	</div>

	<!-- Start Section --> 

	



	<!-- Start Section -->

	<div class="section section-pad section-bg team-section section-gradiant section-fix bg-fixed" id="team">

		<div class="container">

			<div class="row text-center">

				<div class="col-lg-6 offset-lg-3">

					<div class="section-head-s2">

						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0"></h6>

						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">The ABEE Foundation Team</h2>

						<p class="animated" data-animate="fadeInUp" data-delay=".2">The ICO Crypto Team combines a passion for esports, industry experise &amp; proven record in finance, development, marketing &amp; licensing.</p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

            

            <div class="row justify-content-center">

				<div class="col-lg-9 col-md-12">

					<div class="row">



						<div class="col-lg-6 col-md-6 col-xl-4 d-inline-flex justify-content-center">

							<div class="team-member animated" data-animate="fadeInUp" data-delay=".1">

								<div class="team-photo">

									<img src="images/team-e-sq.jpg" alt="team">

									<a href="#team-profile-5" class="expand-trigger content-popup"></a>

								</div>

								<div class="team-info">

									<h5 class="team-name">Dylan <br>Finch</h5>

									<span class="team-title">Board Advisor</span>

									<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

								</div>



								<!-- Start .team-profile  -->

								<div id="team-profile-5" class="team-profile mfp-hide">

									<button title="Close (Esc)" type="button" class="mfp-close">×</button>

									<div class="container-fluid">

										<div class="row no-mg">

											<div class="col-md-6">

												<div class="team-profile-photo">

													<img src="images/team-e-lg.jpg" alt="team" />

												</div>

											</div><!-- .col  -->



											<div class="col-md-6">

												<div class="team-profile-info">

													<h3 class="name">Dylan Finch</h3>

													<p class="sub-title">Board Advisor</p>

													<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

													<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

													<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

												

												</div>

											</div><!-- .col  -->



										</div><!-- .row  -->

									</div><!-- .container  -->

								</div><!-- .team-profile  -->



							</div>

						</div><!-- .col  -->



						<div class="col-lg-6 col-md-6 col-xl-4 d-inline-flex justify-content-center">

							<div class="team-member animated" data-animate="fadeInUp" data-delay=".2">

								<div class="team-photo">

									<img src="images/team-f-sq.jpg" alt="team">

									<a href="#team-profile-6" class="expand-trigger content-popup"></a>

								</div>

								<div class="team-info">

									<h5 class="team-name">Julian <br>Paten</h5>

									<span class="team-title">Board Advisor</span>

									<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

								</div>



								<!-- Start .team-profile  -->

								<div id="team-profile-6" class="team-profile mfp-hide">

									<button title="Close (Esc)" type="button" class="mfp-close">×</button>

									<div class="container-fluid">

										<div class="row no-mg">

											<div class="col-md-6">

												<div class="team-profile-photo">

													<img src="images/team-f-lg.jpg" alt="team" />

												</div>

											</div><!-- .col  -->



											<div class="col-md-6">

												<div class="team-profile-info">

													<h3 class="name">Julian Paten</h3>

													<p class="sub-title">Board Advisor</p>

													<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

													<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

													<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

													

												</div>

											</div><!-- .col  -->



										</div><!-- .row  -->

									</div><!-- .container  -->

								</div><!-- .team-profile  -->



							</div>

						</div><!-- .col  -->



						<div class="col-lg-6 col-md-6 col-xl-4 offset-md-3 offset-lg-3 offset-xl-0 d-inline-flex justify-content-center">

							<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

								<div class="team-photo">

									<img src="images/team-g-sq.jpg" alt="team">

									<a href="#team-profile-7" class="expand-trigger content-popup"></a>

								</div>

								<div class="team-info">

									<h5 class="team-name">Jaxon <br>Kilburn</h5>

									<span class="team-title">Board Advisor</span>

									<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

								</div>



								<!-- Start .team-profile  -->

								<div id="team-profile-7" class="team-profile mfp-hide">

									<button title="Close (Esc)" type="button" class="mfp-close">×</button>

									<div class="container-fluid">

										<div class="row no-mg">

											<div class="col-md-6">

												<div class="team-profile-photo">

													<img src="images/team-g-lg.jpg" alt="team" />

												</div>

											</div><!-- .col  -->



											<div class="col-md-6">

												<div class="team-profile-info">

													<h3 class="name">Jaxon Kilburn</h3>

													<p class="sub-title">Board Advisor</p>

													<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

													<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

													<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

													

												</div>

											</div><!-- .col  -->



										</div><!-- .row  -->

									</div><!-- .container  -->

								</div><!-- .team-profile  -->



							</div>

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .col  -->

			</div><!-- .row  -->

            

            

            <div class="row justify-content-center">

				<div class="col-lg-9 col-md-12">

					<div class="row">



						<div class="col-lg-6 col-md-6 col-xl-4 d-inline-flex justify-content-center">

							<div class="team-member animated" data-animate="fadeInUp" data-delay=".1">

								<div class="team-photo">

									<img src="images/team-e-sq.jpg" alt="team">

									<a href="#team-profile-5" class="expand-trigger content-popup"></a>

								</div>

								<div class="team-info">

									<h5 class="team-name">Dylan <br>Finch</h5>

									<span class="team-title">Board Advisor</span>

									<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

								</div>



								<!-- Start .team-profile  -->

								<div id="team-profile-5" class="team-profile mfp-hide">

									<button title="Close (Esc)" type="button" class="mfp-close">×</button>

									<div class="container-fluid">

										<div class="row no-mg">

											<div class="col-md-6">

												<div class="team-profile-photo">

													<img src="images/team-e-lg.jpg" alt="team" />

												</div>

											</div><!-- .col  -->



											<div class="col-md-6">

												<div class="team-profile-info">

													<h3 class="name">Dylan Finch</h3>

													<p class="sub-title">Board Advisor</p>

													<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

													<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

													<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

													

												</div>

											</div><!-- .col  -->



										</div><!-- .row  -->

									</div><!-- .container  -->

								</div><!-- .team-profile  -->



							</div>

						</div><!-- .col  -->



						<div class="col-lg-6 col-md-6 col-xl-4 d-inline-flex justify-content-center">

							<div class="team-member animated" data-animate="fadeInUp" data-delay=".2">

								<div class="team-photo">

									<img src="images/team-f-sq.jpg" alt="team">

									<a href="#team-profile-6" class="expand-trigger content-popup"></a>

								</div>

								<div class="team-info">

									<h5 class="team-name">Julian <br>Paten</h5>

									<span class="team-title">Board Advisor</span>

									<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

								</div>



								<!-- Start .team-profile  -->

								<div id="team-profile-6" class="team-profile mfp-hide">

									<button title="Close (Esc)" type="button" class="mfp-close">×</button>

									<div class="container-fluid">

										<div class="row no-mg">

											<div class="col-md-6">

												<div class="team-profile-photo">

													<img src="images/team-f-lg.jpg" alt="team" />

												</div>

											</div><!-- .col  -->



											<div class="col-md-6">

												<div class="team-profile-info">

													<h3 class="name">Julian Paten</h3>

													<p class="sub-title">Board Advisor</p>

													<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

													<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

													<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

													

												</div>

											</div><!-- .col  -->



										</div><!-- .row  -->

									</div><!-- .container  -->

								</div><!-- .team-profile  -->



							</div>

						</div><!-- .col  -->



						<div class="col-lg-6 col-md-6 col-xl-4 offset-md-3 offset-lg-3 offset-xl-0 d-inline-flex justify-content-center">

							<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

								<div class="team-photo">

									<img src="images/team-g-sq.jpg" alt="team">

									<a href="#team-profile-7" class="expand-trigger content-popup"></a>

								</div>

								<div class="team-info">

									<h5 class="team-name">Jaxon <br>Kilburn</h5>

									<span class="team-title">Board Advisor</span>

									<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

								</div>



								<!-- Start .team-profile  -->

								<div id="team-profile-7" class="team-profile mfp-hide">

									<button title="Close (Esc)" type="button" class="mfp-close">×</button>

									<div class="container-fluid">

										<div class="row no-mg">

											<div class="col-md-6">

												<div class="team-profile-photo">

													<img src="images/team-g-lg.jpg" alt="team" />

												</div>

											</div><!-- .col  -->



											<div class="col-md-6">

												<div class="team-profile-info">

													<h3 class="name">Jaxon Kilburn</h3>

													<p class="sub-title">Board Advisor</p>

													<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

													<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

													<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

													

												</div>

											</div><!-- .col  -->



										</div><!-- .row  -->

									</div><!-- .container  -->

								</div><!-- .team-profile  -->



							</div>

						</div><!-- .col  -->

					</div><!-- .row  -->

				</div><!-- .col  -->

			</div><!-- .row  -->

            

            

			<div class="row">

				<div class="col-md-12">

					<div class="gaps size-2x"></div>

					<h3 class="sub-heading">ABEE Rideshare Team</h3>

					<div class="gaps size-2x"></div>

				</div>

			</div>

            

            

            

			<div class="row">



				    <div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

											

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->

                	<div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

											

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->

                	<div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

										 

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->

                	<div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

											

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->



			

			</div><!-- .row  -->

			

            

            

            

			<div class="row">



				<div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

											

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->

                <div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

											

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->

                <div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

											

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->

                <div class="col-md-6 col-lg-4 col-xl-3 d-inline-flex justify-content-center">

					<div class="team-member animated" data-animate="fadeInUp" data-delay=".3">

						<div class="team-photo">

							<img src="images/team-b-sq.jpg" alt="team">

							<a href="#team-profile-2" class="expand-trigger content-popup"></a>

						</div>

						<div class="team-info">

							<h5 class="team-name">Stefan <br>Harary</h5>

							<span class="team-title">CTO & Senior Developer</span>

							<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

						</div>



						<!-- Start .team-profile  -->

						<div id="team-profile-2" class="team-profile mfp-hide">

							<button title="Close (Esc)" type="button" class="mfp-close">×</button>

							<div class="container-fluid">

								<div class="row no-mg">

									<div class="col-md-6">

										<div class="team-profile-photo">

											<img src="images/team-b-lg.jpg" alt="team" />

										</div>

									</div><!-- .col  -->



									<div class="col-md-6">

										<div class="team-profile-info">

											<h3 class="name">Stefan Harary</h3>

											<p class="sub-title">CTO & Senior Developer</p>

											<ul class="tpi-social">

												

												<li><a href=""><em class="fab fa-linkedin-in"></em></a></li>

												<li><a href=""><em class="fab fa-"></em></a></li>

											</ul>

											<p>He is a great man to work Lorem ipsum dolor sit amet, consec tetur adipis icing elit. Simi lique, autem. </p>

											<p>Tenetur quos facere magnam volupt ates quas esse Sedrep ell endus mole stiae tates quas esse Sed repell endus molesti aela uda ntium quis quam iusto minima thanks.</p>

										

										</div>

									</div><!-- .col  -->



								</div><!-- .row  -->

							</div><!-- .container  -->

						</div><!-- .team-profile  -->



					</div>

				</div><!-- .col  -->



			

			</div><!-- .row  -->

			

            

            

		</div>

	</div>

	<!-- Start Section -->





	<!-- Start Section -->

	<div class="section section-pad faq-section section-pro no-pb section-bg" id="faq">

		<div class="container">

			<div class="row text-center">

				<div class="col-lg-6 offset-lg-3">

					<div class="section-head-s2">

						<h6 class="heading-xs animated" data-animate="fadeInUp" data-delay=".0">Faqs</h6>

						<h2 class="section-title animated" data-animate="fadeInUp" data-delay=".1">Frequently Asked Questions</h2>

						<p class="animated" data-animate="fadeInUp" data-delay=".2"></p>

					</div>

				</div><!-- .col  -->

			</div><!-- .row  -->

			<div class="row">

				<div class="col-md-12">

					<div class="tab-custom tab-custom-s2">

						<!-- Nav tabs -->

						<ul class="nav nav-tabs text-center animated" data-animate="fadeInUp" data-delay=".15">

							<li class="nav-item">

								<a class="nav-link active" data-toggle="tab" href="#tab-1">General</a>

							</li>

							<li class="nav-item">

								<a class="nav-link" data-toggle="tab" href="#tab-2">Pre-TGE &amp; TGE</a>

							</li>

							<!--<li class="nav-item">

								<a class="nav-link" data-toggle="tab" href="#tab-3">Tokens</a>

							</li>

							<li class="nav-item">

								<a class="nav-link" data-toggle="tab" href="#tab-4">CLIENT</a>

							</li>

							<li class="nav-item">

								<a class="nav-link" data-toggle="tab" href="#tab-5">Legal</a>

							</li>-->

						</ul>

						<!-- Tab panes -->

						<div class="tab-content">

							<div class="tab-pane fade show active" id="tab-1">

								<div class="row">

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">

											<h5>What is The ABEE Foundation and how is it related to ABEE Rideshare?</h5>

											<p>The ABEE Foundation is a non US entity established in Saint Kitts strategically to have a seamless international reach. ABEE Rideshare is the first among many companies that will be using the ABEE name and will be receiving equity free funding from T.A.F </p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">

											<h5>Why is The ABEE Foundation giving equity free funding?</h5>

											<p>The ABEE Foundation is a philanthropic enterprise, We act as a think tank and funding mechanism for ABEE companies world wide. We provide the funding to teams that are hand selected in order to grow the good faith mission of providing the most ethical, balanced and fair sharing economy platforms worldwide.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">

											<h5>How will The ABEE Foundation be able to keep giving equity free capital?</h5>

											<p>Although The ABEE foundation does gives equity free capital to ABEE companies, The ABEE foundation does gain equity in other hand selected and innovative companies. On ABEE Sharing platforms worldwide there is a strategic user incentivizing tool called ABEE VC. ABEE VC encourages new users to switch over to ABEE platforms. When users switch over and consistently use ABEE platforms they gain the ability to submit their ideas into a VC seed funding round. The ABEE Foundation will help fund individuals dreams worldwide and will gain equity in these companies, ultimately growing a rich and diverse portfolio for The ABEE Foundation.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">

											<h5>How can i contribute to The ABEE Foundation?</h5>

											<p>Contributors can participate in the Pre and Main TGE by clicking "ABEE Tokens" in the top right of this site or visiting http://token.abeefoundation.org. The TGE will begin with the pre round and followed by 3 rounds for a total of 63% of all ABEE token supply. </p>

										</div>

									</div><!-- .col  -->

								</div><!-- .row  -->

							</div><!-- End tab-pane -->

                            

                            

                            

                            

							<div class="tab-pane fade" id="tab-2">

								<div class="row">

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">

											<h5>Is there a soft and hard cap?</h5>

											<p>The soft cap will be $2,550,000 and the hard cap will be $30,000,000

                                        </div>

									</div><!-- .col  -->

                                        

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">

											<h5>Can anyone participate? Is there any KYC process?</h5>

											<p>Chinese and US citizens are not able to participate due to their governements restrictions on them. KYC will be required once the TGE has concluded.</p>

										</div>

									</div><!-- .col  -->

                                        

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">

											<h5>When will i be able to see my token balance?</h5>

											<p>Your ABT balance will be viewable on your personal account at token.abeefoundation.org once the transaction is complete. At the conclusion of the TGE your ABT balance will be sent to the QTUM address you specified on your token.abeefoundation.org profile.</p>

										</div>

									</div><!-- .col  -->

                                        

                                        

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">

											<h5>I do not have any experience with QRC20s. Where can i safely store my ABEE Tokens?</h5>

											<p>There are a variety of Qtum wallets available, please see https://qtumeco.io/wallet/. Qtum web wallet is similar to MyEtherWallet.</p>

										</div>

									</div><!-- .col  -->

                                        

								</div><!-- .row  -->

							</div><!-- End tab-pane -->

                                

                                

							<div class="tab-pane fade" id="tab-3">

								<div class="row">

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">

											<h5>When she reached the first hills?</h5>

											<p>ICO Crypto - is unique blockchain platform; that is secure, smart and easy-to-use platform, and completely disrupting the way businesses raise capital,  and the way investors buy and sell. </p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">

											<h5>Big Oxmox advised her not to do?</h5>

											<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc itation ullamco.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">

											<h5>Which roasted parts of sentences fly into your mouth?</h5>

											<p>Ut enim ad minim veniam, quis nostrud exerc itation ullamco. Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">

											<h5>Vokalia and Consonantia, there live?</h5>

											<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipis cing elit, quis nostrud exerc itation ullamco.</p>

										</div>

									</div><!-- .col  -->

								</div><!-- .row  -->

							</div><!-- End tab-pane -->

							<div class="tab-pane fade" id="tab-4">

								<div class="row">

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">

											<h5>Who formed us in his own image?</h5>

											<p>Ut enim ad minim veniam, quis nostrud exerc itation ullamco. Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">

											<h5>While the lovely valley teems with vapour?</h5>

											<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipis cing elit, quis nostrud exerc itation ullamco.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">

											<h5>Which was created for the bliss of souls like mine?</h5>

											<p>ICO Crypto - is unique blockchain platform; that is secure, smart and easy-to-use platform, and completely disrupting the way businesses raise capital,  and the way investors buy and sell. </p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">

											<h5>Wonderful serenity has taken of my entire soul?</h5>

											<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc itation ullamco.</p>

										</div>

									</div><!-- .col  -->

								</div><!-- .row  -->

							</div><!-- End tab-pane -->

							<div class="tab-pane fade" id="tab-5">

								<div class="row">

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".2">

											<h5>Illustrated magazine and housed in a nice?</h5>

											<p>ICO Crypto - is unique blockchain platform; that is secure, smart and easy-to-use platform, and completely disrupting the way businesses raise capital,  and the way investors buy and sell. </p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".3">

											<h5>Samsa was a travelling salesman and above it there?</h5>

											<p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exerc itation ullamco.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".4">

											<h5>Slightly domed and divided by arches?</h5>

											<p>Ut enim ad minim veniam, quis nostrud exerc itation ullamco. Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

										</div>

									</div><!-- .col  -->

									<div class="col-md-6">

										<div class="single-faq animated" data-animate="fadeInUp" data-delay=".5">

											<h5>One morning, when Gregor Samsa?</h5>

											<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, Lorem ipsum dolor sit amet, consectetur adipis cing elit, quis nostrud exerc itation ullamco.</p>

										</div>

									</div><!-- .col  -->

								</div><!-- .row  -->

							</div><!-- End tab-pane -->

						</div><!-- End tab-content -->

					</div><!-- End tab-custom -->

				</div><!-- .col -->

			</div><!-- .row -->

		</div><!-- End container -->

		<div class="mask-ov-left mask-ov-s5"></div><!-- .mask overlay -->

	</div>

	<!-- End Section -->






	<!-- Start Section -->

	<div class="section subscribe-section-s2 section-bg section-dark" id="join">
		<div class="container">
			<div class="subscribe-box animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="heading-sm-s3">
                            <h5>Don't miss out, Stay updated</h5>
                            <p>Sign up for updates and market news. Subscribe to our newsletter and receive update about ICOs and crypto tips.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="gaps size-2x"></div>
                        <form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form inline-form-s2" abineguid="6B8B90FBC9F0474583EFF0BF32C69E8A" novalidate="novalidate">
                            <input type="text" name="youremail" class="input-round-s2 required email" placeholder="Enter your email address" aria-required="true">
                            <input type="text" class="d-none" name="form-anti-honeypot" value="">
                            <button type="submit" class="btn">Subscribe</button>
                            <div class="subscribe-results"></div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>

	<!-- End Section -->
	

	



	<!-- Start Section -->

	<div class="section section-pad section-bg-dark" id="partners">

		<div class="container">

			<div class="row text-center">

				<div class="col-md-6 offset-md-3">

					<div class="section-head">

						<h6 class="section-title-sm animated" data-animate="fadeInUp" data-delay=".0">As seen in</h6>

					</div>

				</div>

			</div><!-- .row  -->

			<div class="partner-list">

				<div class="row text-center">

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".1">

							<img src="assets/images/PBJ_logo.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".2">

							<img src="assets/images/CN-LOGO.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".3">

							<img src="assets/images/New-Dash-Force-News-Logo.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".4">

							<img src="images/partner-xs-d.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".5">

							<img src="images/partner-xs-e.png" alt="partner">

						</div>

					</div><!-- .col  -->

					<div class="col-sm col-6">

						<div class="single-partner animated" data-animate="fadeInUp" data-delay=".6">

							<img src="images/partner-xs-f.png" alt="partner">

						</div>

					</div><!-- .col  -->

				</div><!-- .row  -->

				<div class="row text-center">

					<div class="col-md-10 offset-md-1">

						<div class="row">

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".7">

									<img src="images/partner-sm-a.png" alt="partner">

								</div>

							</div><!-- .col  -->

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".8">

									<img src="images/partner-sm-b.png" alt="partner">

								</div>

							</div><!-- .col  -->

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay=".9">

									<img src="images/partner-sm-c.png" alt="partner">

								</div>

							</div><!-- .col  -->

							<div class="col-sm col-6">

								<div class="single-partner animated" data-animate="fadeInUp" data-delay="1">

									<img src="images/partner-sm-d.png" alt="partner">

								</div>

							</div><!-- .col  -->

						</div><!-- .row  -->

					</div><!-- .col  -->

				</div><!-- .row  -->

			</div><!-- .partner-list  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section -->

	



	<!-- Start Section -->

	<div class="section footer-scetion footer-particle section-pad-sm section-bg-dark">

		<div class="container">

			<div class="row">

				<div class="col-sm-6 col-xl-4 res-l-bttm">

					<a href="#">

						<img class="logo logo-light" alt="logo" src="images/abee_logo.png" srcset="images/abee_logo.png 2x">

					</a>

					<ul class="social">

						<!--<li class="animated" data-animate="fadeInUp" data-delay=".1"><a href="#"><em class="fab fa-facebook-f"></em></a></li>-->

						<li class="animated" data-animate="fadeInUp" data-delay=".2"><a href="#"><em class="fab fa-twitter"></em></a></li>

						<!--<li class="animated" data-animate="fadeInUp" data-delay=".3"><a href="#"><em class="fab fa-youtube"></em></a></li>-->

						<li class="animated" data-animate="fadeInUp" data-delay=".4"><a href="https://github.com/abeefoundation"><em class="fab fa-github"></em></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay=".5"><a href="#"><em class="fab fa-bitcoin"></em></a></li>

						<li class="animated" data-animate="fadeInUp" data-delay=".6"><a href="https://medium.com/@abeefoundation"><em class="fab fa-medium-m"></em></a></li>

					</ul>

				</div><!-- .col  -->

				<div class="col-sm-6 col-xl-4 res-l-bttm">

					<div class="footer-subscription">

						<h6 class="animated" data-animate="fadeInUp" data-delay=".6">Subscribe to our newsletter</h6>

						<form id="subscribe-form" action="form/subscribe.php" method="post" class="subscription-form animated" data-animate="fadeInUp" data-delay=".7">

							<input type="text" name="youremail" class="input-round required email" placeholder="Enter your email" >

							<input type="text" class="d-none" name="form-anti-honeypot" value="">

							<button type="submit" class="btn btn-plane">Subscribe</button>

							<div class="subscribe-results"></div>

						</form>

					</div>

				</div><!-- .col  -->

				<div class="col-xl-4">

					<ul class="link-widget animated" data-animate="fadeInUp" data-delay=".8">

						<li><a href="#intro" class="menu-link">What is ABEE</a></li>

						<li><a href="#apps" class="menu-link">Apps</a></li>

						<li><a href="https://token.abeefoundation.org" class="menu-link">Join TGE</a></li>

						<li><a href="#tokenSale" class="menu-link">TGE</a></li>

						<li><a href="assets/documents/ABEE_Foundation_Main_WP.pdf" class="menu-link">Whitepaper</a></li>

						<li><a href="#contact" class="menu-link">Contact</a></li>

						<li><a href="#roadmap" class="menu-link">Roadmap</a></li>

						<li><a href="#team" class="menu-link">Teams</a></li>

						<li><a href="#faq" class="menu-link">FAQ</a></li>

					</ul>

				</div><!-- .col  -->

			</div><!-- .row  -->

			<div class="gaps size-2x"></div>

			<div class="row">

				<div class="col-md-7">

					<span class="copyright-text animated" data-animate="fadeInUp" data-delay=".9">

						Copyright &copy; 2018, ABEE Foundation.

						<span>All trademarks and copyrights belong to ABEE Foundation.</span>

					</span>

				</div><!-- .col  -->

				<!-- class="col-md-5 text-right mobile-left">

					<ul class="footer-links animated" data-animate="fadeInUp" data-delay="1">

						<li><a href="#">Privacy Policy</a></li>

						<li><a href="#">Terms &amp; Conditions</a></li>

						<li>

							<div class="language-switcher">

								<a href="#" data-toggle="dropdown">EN</a>

								<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

									<a class="dropdown-item" href="#">FR</a>

									<a class="dropdown-item" href="#">CH</a>

									<a class="dropdown-item" href="#">BR</a>

								</div>

							</div>

						</li>

					</ul>

				</div> --> 

			</div><!-- .row  -->

		</div><!-- .container  -->

	</div>

	<!-- End Section -->



	<!-- Preloader !remove please if you do not want -->

	<div id="preloader">

		<div id="loader"></div>

		<div class="loader-section loader-top"></div>

   		<div class="loader-section loader-bottom"></div>

	</div>

	<!-- Preloader End -->



	<!-- JavaScript (include all script here) -->

	<script src="assets/js/jquery.bundle.js?ver=123"></script>

	<script src="assets/js/script.js?ver=123"></script>

	<script src="assets/js/telega.js?v=<?=time();?>"></script>



</body>

</html>

